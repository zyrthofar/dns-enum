# DNS ENUM

This project is a proof-of-concept. It was created as a learning tool on how subdomain enumeration would work using NSEC3 records, and a visual aid during a demonstration. As such, it is not optimized, and there are way better tools if one desires to enumerate a domain's subdomains.

It is separated into three steps:

1) Enumeration of all NSEC3 hashes;
2) Dictionary attack; and
3) Brute-force attack.

Only the first step is considered an "online attack", performing a DNS query for each hash. These are saved in a database, to prevent having to start over everytime.

Steps two and three are "offline attacks", trying to match the NSEC3 hashes that were found in the first step. The domains that are found are also saved in the database.

## Usage

The application is configured via three constants at the top of `main.rs`:

* `BASE_NAME`
* `NAMESERVER`
* `DICTIONARY_FILE`

### Database

The database's connection string is hard-coded in `database.rs`, targeting the PostgreSQL database created via Docker Desktop.

To create and start the database:

```sh
docker compose exec --no-TTY postgres psql -U dns -d dns < schema.sql
docker compose up
```

The `schema.sql` file creates a table named `records`. This is an idea that is currently abandonned, and this table is not populated. If all available records for a given domain name are desired (as reported in the NSEC3 record), these can be found in the table `nsec3_records`'s `rdata` column, as per [RFC 5155](https://www.rfc-editor.org/rfc/rfc5155#section-3.2).

The database can be accessed with an SQL client, or

```sh
docker compose exec postgres psql -U dns -d dns
```

Some useful queries are:

* Get a summary of all zones

```sql
SELECT
    z.id,
    z.name,
    (SELECT COUNT(*) FROM nsec3_records WHERE zone_id = z.id) as nsec3,
    (SELECT COUNT(*) FROM domains WHERE zone_id = z.id) as domains
FROM zones z
ORDER BY z.name;
                  id                  |      name       | nsec3 | domains 
--------------------------------------+-----------------+-------+---------
 c4972036-8872-416d-8e29-082c3553bf79 | example.com.    |     3 |       2
 c34ebb65-bb96-45a4-88c3-d7eeda41af2f | protonmail.com. |   138 |      32
 a77e6874-5d8a-405b-b76d-740fe8e60e09 | verisign.com.   |  1331 |     273
 b1d13d0e-1015-4697-ac43-11f39738d04a | verisign.net.   |   681 |      53
(4 rows)
```

* Get all found subdomains and their associated hashes

```sql
SELECT z.name AS "zone", d.name, r.hash
FROM zones z
INNER JOIN domains d ON d.zone_id = z.id
INNER JOIN nsec3_records r ON r.id = d.nsec3_record_id
ORDER BY z.name, d.name;
      zone       |             name             |                    hash                    
-----------------+------------------------------+--------------------------------------------
 example.com.    | example.com.                 | \xf8cb89209eefc7f4356994b54df84cfa41e6403d
 example.com.    | www.example.com.             | \x9867a1a369fdb2f513255a184383fa27665c037c
 protonmail.com. | account.protonmail.com.      | \x82f823bce714577e3571bae13487e5fe88c4f8fc
 protonmail.com. | api.protonmail.com.          | \x57f4126b79381890e3c7dfd26b41d228c4986492
 protonmail.com. | app.protonmail.com.          | \x0162bc63dc7fc8563ec242185d680ffc7204fb62
 protonmail.com. | autoconfig.protonmail.com.   | \x8ddd955cc2863d08031da0154c9e28e839016883
 protonmail.com. | beta.protonmail.com.         | \x091f904f4f14ba9b5c7c8f7ecd174c9fb9bd78ed
 protonmail.com. | calendar.protonmail.com.     | \x64029a860cc58b680bebbd64ee5b27654b23c1c3
 protonmail.com. | careers.protonmail.com.      | \xad653edad892faea7e7ab04abf9f92cea04c3014
 protonmail.com. | drive.protonmail.com.        | \x5f4c656a6cd43033a992404be2670dff447eb580
 protonmail.com. | get.protonmail.com.          | \xda57081ca0bbe9b1a67e6f6974d992459c985abe
 protonmail.com. | hub.protonmail.com.          | \x7d53bfeafbfb9f7fa86315dcace13d09ec4ecbc6
 protonmail.com. | mail-api.protonmail.com.     | \x5e7a721ed25e000bfdce44baf157dbc067f9b2f0
 protonmail.com. | mail.protonmail.com.         | \x6bc3afd931057b71bae5d3d7b5e6c243cab0aa1a
 protonmail.com. | mta-sts.protonmail.com.      | \xa9a5f661d310ff562ed6bcae02943f8423ddde22
 protonmail.com. | news.protonmail.com.         | \xba4555c6f2a9ba8c54ce8effec4a6e79d20bd962
 protonmail.com. | notify.protonmail.com.       | \x1b59665107ef1c30fc246cae12ec7bf7222e9296
 protonmail.com. | ns1.protonmail.com.          | \x94b53c2558c1b1e140930f1cb205963c12155f2b
 protonmail.com. | ns2.protonmail.com.          | \xbb852914a5f3d5987a25e4d8a293442c29f1191e
 protonmail.com. | ns3.protonmail.com.          | \x72eebbd3d078fdff542b630ce77f0d6773f68292
...
```

* Get all hashes and their associated domains

```sql
SELECT z.name AS "zone", r.hash, d.name
FROM zones z
INNER JOIN nsec3_records r ON r.zone_id = z.id
LEFT JOIN domains d ON d.nsec3_record_id = r.id
ORDER BY z.name, r.hash;
      zone       |                    hash                    |             name             
-----------------+--------------------------------------------+------------------------------
 example.com.    | \x14f3998f5912e03a018a34ddeee7cd1f12a8f107 | 
 example.com.    | \x9867a1a369fdb2f513255a184383fa27665c037c | www.example.com.
 example.com.    | \xf8cb89209eefc7f4356994b54df84cfa41e6403d | example.com.
 protonmail.com. | \x00b4c0d1d74a31f60f072806f43b1d1abbe430f5 | 
 protonmail.com. | \x0162bc63dc7fc8563ec242185d680ffc7204fb62 | app.protonmail.com.
 protonmail.com. | \x058763b63a4d5b7e6f3ef9f72855386980358065 | old.protonmail.com.
 protonmail.com. | \x084ea95c42f7fdf6cec2cd1e3e2ac1955257f850 | 
 protonmail.com. | \x091f904f4f14ba9b5c7c8f7ecd174c9fb9bd78ed | beta.protonmail.com.
 protonmail.com. | \x0a8e38566827c255de915a3849ccb0e259deb0da | 
 protonmail.com. | \x0bd3775b4815f6a9e21d62474281f7dec7a83882 | 
 protonmail.com. | \x0e1ad775ca4cc89e9e2fff127d1e72d7836574e4 | 
 protonmail.com. | \x166b4f948404a10d2ff2868e0beb266f4c158656 | 
 protonmail.com. | \x1ada65e03bfd1ae0f68164848390354f73ea98ed | 
 protonmail.com. | \x1b59665107ef1c30fc246cae12ec7bf7222e9296 | notify.protonmail.com.
 protonmail.com. | \x1c1494bb26dd14452f4963375ee99fbbf2321807 | 
 protonmail.com. | \x1eb12348214e72ed7a018a454e2f98f20492edb1 | 
 protonmail.com. | \x2325b6bc576966be244f291fbbbf081c0c5b9579 | recovery.protonmail.com.
 protonmail.com. | \x24b11bb9e795036281e890be068084a8e8190847 | 
 protonmail.com. | \x25a47dabcb5996fdb56717aadb907ab009a5c511 | 
 protonmail.com. | \x281dba881a2f6af47a00a4e2584457441962634d | 
...
```
