CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

CREATE OR REPLACE FUNCTION set_created_at() RETURNS TRIGGER AS $$
BEGIN
    NEW.created_at = clock_timestamp();
    NEW.updated_at = clock_timestamp();
    RETURN NEW;
END;
$$ language 'plpgsql';

CREATE OR REPLACE FUNCTION set_updated_at() RETURNS TRIGGER AS $$
BEGIN
    NEW.updated_at = clock_timestamp();
    RETURN NEW;
END;
$$ language 'plpgsql';

DROP TABLE IF EXISTS "records";
DROP TABLE IF EXISTS "domains";
DROP TABLE IF EXISTS "nsec3_records";
DROP TABLE IF EXISTS "zones";

CREATE TABLE "zones" (
    "id"         UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    "name"       varchar(253) NOT NULL,
    "nsec3_alg"  integer,
    "nsec3_its"  integer,
    "nsec3_salt" bytea,
    "created_at" timestamp,
    "updated_at" timestamp
);
CREATE UNIQUE INDEX "zones_name_idx" ON "zones" ("name");
CREATE TRIGGER "set_created_at" BEFORE INSERT ON "zones" FOR EACH ROW EXECUTE PROCEDURE set_created_at();
CREATE TRIGGER "set_updated_at" BEFORE UPDATE ON "zones" FOR EACH ROW EXECUTE PROCEDURE set_updated_at();

CREATE TABLE "nsec3_records" (
    "id"         UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    "zone_id"    UUID NOT NULL REFERENCES "zones" (id),
    "hash"       bytea NOT NULL, -- 160b
    "next_hash"  bytea NOT NULL, -- 160b
    "rdata"      bytea NOT NULL,
    "created_at" timestamp,
    "updated_at" timestamp
);
CREATE UNIQUE INDEX "nsec3_records_hash_idx" ON "nsec3_records" ("zone_id", "hash");
CREATE TRIGGER "set_created_at" BEFORE INSERT ON "nsec3_records" FOR EACH ROW EXECUTE PROCEDURE set_created_at();
CREATE TRIGGER "set_updated_at" BEFORE UPDATE ON "nsec3_records" FOR EACH ROW EXECUTE PROCEDURE set_updated_at();

CREATE TABLE "domains" (
    "id"              UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    "zone_id"         UUID NOT NULL REFERENCES "zones" (id),
    "nsec3_record_id" UUID REFERENCES "nsec3_records" (id),
    "name"            varchar(253) NOT NULL,
    "created_at"      timestamp,
    "updated_at"      timestamp
);
CREATE INDEX "domains_zone_id_idx" ON "domains" ("zone_id");
CREATE INDEX "domains_nsec3_record_id_idx" ON "domains" ("nsec3_record_id");
CREATE UNIQUE INDEX "domains_name_idx" ON "domains" ("zone_id", "name");
CREATE TRIGGER "set_created_at" BEFORE INSERT ON "domains" FOR EACH ROW EXECUTE PROCEDURE set_created_at();
CREATE TRIGGER "set_updated_at" BEFORE UPDATE ON "domains" FOR EACH ROW EXECUTE PROCEDURE set_updated_at();

CREATE TABLE "records" (
    "id"         UUID PRIMARY KEY DEFAULT uuid_generate_v4(),
    "domain_id"  UUID NOT NULL REFERENCES "domains" (id),
    "name"       varchar(253) NOT NULL,
    "type"       varchar(32) NOT NULL, --integer NOT NULL, -- 16b
    "class"      varchar(32) NOT NULL, --integer NOT NULL, -- 16b
    "ttl"        integer NOT NULL, -- 32b
    "rdata"      bytea NOT NULL,
    "created_at" timestamp,
    "updated_at" timestamp
);
CREATE INDEX "records_domain_id_idx" ON "records" ("domain_id");
CREATE TRIGGER "set_created_at" BEFORE INSERT ON "records" FOR EACH ROW EXECUTE PROCEDURE set_created_at();
CREATE TRIGGER "set_updated_at" BEFORE UPDATE ON "records" FOR EACH ROW EXECUTE PROCEDURE set_updated_at();
