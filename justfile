lint:
    @cargo fmt
    @cargo clippy -- -W clippy::complexity -W clippy::pedantic -W clippy::perf -W clippy::style

test:
    @cargo test
