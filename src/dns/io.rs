use std::io::{Read, Write};

pub trait ByteRead: Read {
    /// # Errors
    ///
    /// Will return `Err` if the bytes could not be read.
    fn read(&mut self, buf: &mut [u8]) -> Result<(), String>;

    /// # Errors
    ///
    /// Will return `Err` if the bytes could not be read.
    fn read_u8(&mut self) -> Result<u8, String>;

    /// # Errors
    ///
    /// Will return `Err` if the bytes could not be read.
    fn read_u16(&mut self) -> Result<u16, String>;

    /// # Errors
    ///
    /// Will return `Err` if the bytes could not be read.
    fn read_u32(&mut self) -> Result<u32, String>;
}

impl<R> ByteRead for R
where
    R: Read,
{
    fn read(&mut self, buf: &mut [u8]) -> Result<(), String> {
        self.read_exact(&mut buf[..])
            .map_err(|e: std::io::Error| format!("error reading bytes: {e}"))
    }

    fn read_u8(&mut self) -> Result<u8, String> {
        let mut buf = [0u8];
        ByteRead::read(self, &mut buf[..])?;

        Ok(buf[0])
    }

    fn read_u16(&mut self) -> Result<u16, String> {
        let mut buf = [0u8, 0];
        ByteRead::read(self, &mut buf[..])?;

        Ok((u16::from(buf[0]) << 8) + u16::from(buf[1]))
    }

    fn read_u32(&mut self) -> Result<u32, String> {
        let mut buf = [0u8, 0, 0, 0];
        ByteRead::read(self, &mut buf[..])?;

        Ok((u32::from(buf[0]) << 24)
            + (u32::from(buf[1]) << 16)
            + (u32::from(buf[2]) << 8)
            + u32::from(buf[3]))
    }
}

pub trait ByteWrite: Write {
    fn write(&mut self, buf: &[u8]);
    fn write_u8(&mut self, v: u8);
    fn write_u16(&mut self, v: u16);
    fn write_u32(&mut self, v: u32);
}

impl<W> ByteWrite for W
where
    W: Write,
{
    fn write(&mut self, buf: &[u8]) {
        self.write_all(buf).expect("write to vec buffer");
    }

    fn write_u8(&mut self, value: u8) {
        let buf = [value];
        ByteWrite::write(self, &buf);
    }

    fn write_u16(&mut self, value: u16) {
        let buf = [((value & 0xff00) >> 8) as u8, (value & 0x00ff) as u8];
        ByteWrite::write(self, &buf);
    }

    fn write_u32(&mut self, value: u32) {
        let buf = [
            ((value & 0xff00_0000) >> 24) as u8,
            ((value & 0x00ff_0000) >> 16) as u8,
            ((value & 0x0000_ff00) >> 8) as u8,
            (value & 0x0000_00ff) as u8,
        ];
        ByteWrite::write(self, &buf);
    }
}

#[cfg(test)]
mod tests {
    #[test]
    fn test_read() {
        use crate::io::ByteRead;

        let mut source: &[u8] = [0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07, 0x08].as_slice();
        let mut target = [0u8; 3];

        let result = source.read(&mut target);
        assert!(result.is_ok());
        assert_eq!(source, [0x04, 0x05, 0x06, 0x07, 0x08]);
        assert_eq!(target, [0x01, 0x02, 0x03]);

        let result = source.read(&mut target);
        assert!(result.is_ok());
        assert_eq!(source, [0x07, 0x08]);
        assert_eq!(target, [0x04, 0x05, 0x06]);

        let result = source.read(&mut target);
        assert!(result.is_err());
        assert_eq!(
            result.err(),
            Some("error reading bytes: failed to fill whole buffer".to_string())
        );
        assert_eq!(source, [0x07, 0x08]);
        assert_eq!(target, [0x04, 0x05, 0x06]);
    }

    #[test]
    fn test_read_u8() {
        use crate::io::ByteRead;

        let mut source: &[u8] = [0x01, 0x02, 0x03, 0x04, 0x05].as_slice();

        let result = source.read_u8();
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), 0x01);
        assert_eq!(source, [0x02, 0x03, 0x04, 0x05]);
    }

    #[test]
    fn test_read_u16() {
        use crate::io::ByteRead;

        let mut source: &[u8] = [0x01, 0x02, 0x03, 0x04, 0x05].as_slice();

        let result = source.read_u16();
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), 0x0102);
        assert_eq!(source, [0x03, 0x04, 0x05]);
    }

    #[test]
    fn test_read_u32() {
        use crate::io::ByteRead;

        let mut source: &[u8] = [0x01, 0x02, 0x03, 0x04, 0x05].as_slice();

        let result = source.read_u32();
        assert!(result.is_ok());
        assert_eq!(result.unwrap(), 0x01020304);
        assert_eq!(source, [0x05]);
    }

    #[test]
    fn test_write() {
        use crate::io::ByteWrite;

        let mut source: Vec<u8> = vec![0x01, 0x02];

        source.write([0x03, 0x04, 0x05].as_slice());
        assert_eq!(source, [0x01, 0x02, 0x03, 0x04, 0x05]);

        source.write([0x06, 0x07].as_slice());
        assert_eq!(source, [0x01, 0x02, 0x03, 0x04, 0x05, 0x06, 0x07]);
    }

    #[test]
    fn test_write_u8() {
        use crate::io::ByteWrite;

        let mut source: Vec<u8> = vec![0x01, 0x02];

        source.write_u8(0x03);
        assert_eq!(source, [0x01, 0x02, 0x03]);
    }

    #[test]
    fn test_write_u16() {
        use crate::io::ByteWrite;

        let mut source: Vec<u8> = vec![0x01, 0x02];

        source.write_u16(0x0304);
        assert_eq!(source, [0x01, 0x02, 0x03, 0x04]);
    }

    #[test]
    fn test_write_u32() {
        use crate::io::ByteWrite;

        let mut source: Vec<u8> = vec![0x01, 0x02];

        source.write_u32(0x03040506);
        assert_eq!(source, [0x01, 0x02, 0x03, 0x04, 0x05, 0x06]);
    }
}
