use super::class::Class;
use super::io::ByteWrite;
use super::name::Name;
use super::r#type::Type;

// 4.1.3. Resource record format
//
// The answer, authority, and additional sections all share the same
// format: a variable number of resource records, where the number of
// records is specified in the corresponding count field in the header.
// Each resource record has the following format:
//                                     1  1  1  1  1  1
//       0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                                               |
//     /                                               /
//     /                      NAME                     /
//     |                                               |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                      TYPE                     |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                     CLASS                     |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                      TTL                      |
//     |                                               |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                   RDLENGTH                    |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--|
//     /                     RDATA                     /
//     /                                               /
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//
// where:
//
// NAME            a domain name to which this resource record pertains.
//
// TYPE            two octets containing one of the RR type codes.  This
//                 field specifies the meaning of the data in the RDATA
//                 field.
//
// CLASS           two octets which specify the class of the data in the
//                 RDATA field.
//
// TTL             a 32 bit unsigned integer that specifies the time
//                 interval (in seconds) that the resource record may be
//                 cached before it should be discarded.  Zero values are
//                 interpreted to mean that the RR can only be used for the
//                 transaction in progress, and should not be cached.
//
// RDLENGTH        an unsigned 16 bit integer that specifies the length in
//                 octets of the RDATA field.
//
// RDATA           a variable length string of octets that describes the
//                 resource.  The format of this information varies
//                 according to the TYPE and CLASS of the resource record.
//                 For example, the if the TYPE is A and the CLASS is IN,
//                 the RDATA field is a 4 octet ARPA Internet address.

#[derive(Clone, Debug, Eq, PartialEq)]
// #[cfg_attr(test, derive(Clone, Debug, Eq, PartialEq))]
pub struct ResourceRecord {
    pub name: Name,
    pub r#type: Type,
    pub class: Class,
    pub ttl: u32,
    pub rdata: Vec<u8>,
    // pub rdata: String,
}

// impl TryFrom<Rc<Cursor<Vec<u8>>>> for ResourceRecord {
//     type Error = Error;

//     fn try_from(mut v: Rc<Cursor<Vec<u8>>>) -> Result<Self, Self::Error> {
//         let mut rr = ResourceRecord {
//             name: Name::try_from(v)?,
//             _type: Type::try_from(v.get_u16())?,
//             class: Class::try_from(v.get_u16())?,
//             ttl: v.get_u32(),
//             rdata: "".to_string(),
//         };

//         let rdlength = v.get_u16();
//         // let mut rdata = v.split_to(rdlength as usize);

//         match rr._type {
//             // Type::A => {
//             //     assert_eq!(rdlength, 4);
//             //     rr.rdata = format!("{}.{}.{}.{}", v.get_u8(), v.get_u8(), v.get_u8(), v.get_u8());
//             // },
//             // Type::NS => {
//             //     let name = Name::try_from(v)?;
//             //     rr.rdata = name.to_string();
//             // },
//             // // Type::MD => ,
//             // // Type::MF => ,
//             // Type::CNAME |
//             // Type::PTR => {
//             //     let name = Name::try_from(v)?;
//             //     rr.rdata = name.to_string();
//             // },
//             // Type::SOA => {
//             //     // b"\x03kim\x02ns\ncloudflare\xc0\x1d\x03dns\xc09\x89@\xfa}\0\0'\x10\0\0\t`\0\t:\x80\0\0\x0e\x10"
//             //     // kim.ns.cloudflare.com. dns.cloudflare.com. 2302737021 10000 2400 604800 3600
//             //     let mname = Name::try_from(v)?;
//             //     let rname = Name::try_from(v)?;
//             //     let serial = v.get_u32();
//             //     let refresh = v.get_i32();
//             //     let retry = v.get_i32();
//             //     let expire = v.get_i32();
//             //     let minimum = v.get_u32();
//             //     rr.rdata = format!("{} {} {} {} {} {} {}", mname, rname, serial, refresh, retry, expire, minimum);
//             // },
//             // Type::MB => ,
//             // Type::MG => ,
//             // Type::MR => ,
//             // Type::NULL => ,
//             // Type::WKS => ,
//             // Type::HINFO => ,
//             // Type::MINFO => ,
//             // Type::MX => ,
//             // Type::TXT => ,
//             // Type::AXFR => ,
//             // Type::MAILB => ,
//             // Type::MAILA => ,
//             // Type::ALL => ,
//             // Type::Unknown(u16) => ,
//             _ => {
//                 rr.rdata = format!("{:?}", v.take(rdlength as usize))
//             },
//         }

//         Ok(rr)
//     }
// }

impl std::fmt::Display for ResourceRecord {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.name)?;
        write!(f, " {}", self.class)?;
        write!(f, " {}", self.r#type)?;
        write!(f, ", ttl {}s", self.ttl)?;
        // write!(f, ", {}", self.rdata)?;
        Ok(())
    }
}

impl From<&ResourceRecord> for Vec<u8> {
    #[allow(clippy::cast_possible_truncation)]
    #[allow(clippy::cast_sign_loss)]
    fn from(v: &ResourceRecord) -> Self {
        let mut bytes: Vec<u8> = Vec::new();

        bytes.write(Vec::<u8>::from(&v.name).as_slice());
        bytes.write_u16(u16::from(v.r#type));
        bytes.write_u16(u16::from(v.class));
        bytes.write_u32(v.ttl);
        bytes.write_u16(v.rdata.len() as u16);
        bytes.write(v.rdata.as_slice());

        bytes
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_display() {
        let resource_record = &ResourceRecord {
            name: Name::new("example.com."),
            r#type: Type::A,
            class: Class::IN,
            ttl: 3600,
            rdata: vec![0x01, 0x23, 0x45, 0x67],
        };

        assert_eq!(
            resource_record.to_string(),
            "example.com. IN A, ttl 3600s".to_string()
        );
    }

    #[test]
    fn test_into_vec_u8() {
        let resource_record = &ResourceRecord {
            name: Name::new("example.com."),
            r#type: Type::A,
            class: Class::IN,
            ttl: 3600,
            rdata: vec![0x01, 0x23, 0x45, 0x67],
        };

        assert_eq!(
            Vec::<u8>::from(resource_record),
            b"\x07example\x03com\x00\x00\x01\x00\x01\x00\x00\x0e\x10\x00\x04\x01\x23\x45\x67"
                .as_slice(),
        );
    }
}
