// A four bit field that specifies kind of query in this message. This
// value is set by the originator of a query and copied into the
// response.  The values are:
//     0    a standard query (QUERY)
//     1    an inverse query (IQUERY)
//     2    a server status request (STATUS)
//     3-15 reserved for future use

#[derive(Default, Copy, Clone, Eq, PartialEq, Debug)]
pub enum Opcode {
    #[default]
    Query,
    IQuery,
    Status,
}

impl TryFrom<u8> for Opcode {
    type Error = String;

    fn try_from(v: u8) -> Result<Self, String> {
        // Opcode field is 4 bits long. The other bits are ignored.
        match v & 0b_0000_1111 {
            0 => Ok(Self::Query),
            1 => Ok(Self::IQuery),
            2 => Ok(Self::Status),
            _ => Err("error with opcode: invalid value".to_string()),
        }
    }
}

impl TryFrom<u16> for Opcode {
    type Error = String;

    fn try_from(v: u16) -> Result<Self, String> {
        Opcode::try_from(u8::try_from(v).map_err(|_e| "error casting opcode to u8".to_string())?)
    }
}

impl From<Opcode> for u8 {
    fn from(v: Opcode) -> Self {
        match v {
            Opcode::Query => 0,
            Opcode::IQuery => 1,
            Opcode::Status => 2,
        }
    }
}

impl From<Opcode> for u16 {
    fn from(v: Opcode) -> Self {
        u16::from(u8::from(v))
    }
}

impl std::fmt::Display for Opcode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Self::Query => "QUERY".to_string(),
            Self::IQuery => "IQUERY".to_string(),
            Self::Status => "STATUS".to_string(),
        };
        write!(f, "{s}")?;
        Ok(())
    }
}
