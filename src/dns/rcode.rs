// Response code - this 4 bit field is set as part of responses.  The
// values have the following interpretation:
//     0    No error condition
//     1    Format error - The name server was unable to interpret the
//          query.
//     2    Server failure - The name server was unable to process this
//          query due to a problem with the name server.
//     3    Name Error - Meaningful only for responses from an
//          authoritative name server, this code signifies that the
//          domain name referenced in the query does not exist.
//     4    Not Implemented - The name server does not support the
//          requested kind of query.
//     5    Refused - The name server refuses to perform the specified
//          operation for policy reasons.  For example, a name server
//          may not wish to provide the information to the particular
//          requester, or a name server may not wish to perform a
//          particular operation (e.g., zone transfer) for particular
//          data.
//     6-15 Reserved for future use.

#[derive(Default, Copy, Clone, Eq, PartialEq, Debug)]
pub enum RCode {
    #[default]
    NoError,
    FormatError,
    ServerFailure,
    NameError,
    NotImplemented,
    Refused,
}

impl TryFrom<u8> for RCode {
    type Error = String;

    fn try_from(v: u8) -> Result<Self, String> {
        // Opcode field is 4 bits long. The other bits are ignored.
        match v & 0b_0000_1111 {
            0 => Ok(Self::NoError),
            1 => Ok(Self::FormatError),
            2 => Ok(Self::ServerFailure),
            3 => Ok(Self::NameError),
            4 => Ok(Self::NotImplemented),
            5 => Ok(Self::Refused),
            _ => Err("error with rcode: invalid value".to_string()),
        }
    }
}

impl TryFrom<u16> for RCode {
    type Error = String;

    fn try_from(v: u16) -> Result<Self, String> {
        RCode::try_from(
            u8::try_from(v)
                .map_err(|_e| "error with rcode: cannot cast value to u8".to_string())?,
        )
    }
}

impl From<RCode> for u8 {
    fn from(v: RCode) -> Self {
        match v {
            RCode::NoError => 0,
            RCode::FormatError => 1,
            RCode::ServerFailure => 2,
            RCode::NameError => 3,
            RCode::NotImplemented => 4,
            RCode::Refused => 5,
        }
    }
}

impl From<RCode> for u16 {
    fn from(v: RCode) -> Self {
        u16::from(u8::from(v))
    }
}

impl std::fmt::Display for RCode {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match self {
            Self::NoError => "NoError".to_string(),
            Self::FormatError => "FormatError".to_string(),
            Self::ServerFailure => "ServerFailure".to_string(),
            Self::NameError => "NameError".to_string(),
            Self::NotImplemented => "NotImplemented".to_string(),
            Self::Refused => "Refused".to_string(),
        };
        write!(f, "{s}")?;
        Ok(())
    }
}
