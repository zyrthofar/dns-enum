#[derive(Default, Copy, Clone, Eq, PartialEq, Hash, Debug)]
#[allow(clippy::upper_case_acronyms)]
pub enum Class {
    #[default]
    IN, //              1 the Internet
    CS, //              2 the CSNET class (Obsolete - used only for examples in some obsolete RFCs)
    CH, //              3 the CHAOS class
    HS, //              4 Hesiod [Dyer 87]
    ALL, //             255 any class
}

impl TryFrom<u16> for Class {
    type Error = String;

    fn try_from(v: u16) -> Result<Self, String> {
        match v {
            1 => Ok(Class::IN),
            2 => Ok(Class::CS),
            3 => Ok(Class::CH),
            4 => Ok(Class::HS),
            255 => Ok(Class::ALL),
            _ => Err("error with class: invalid value".to_string()),
        }
    }
}

impl From<Class> for u16 {
    fn from(v: Class) -> Self {
        match v {
            Class::IN => 1,
            Class::CS => 2,
            Class::CH => 3,
            Class::HS => 4,
            Class::ALL => 255,
        }
    }
}

impl std::fmt::Display for Class {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match *self {
            Class::IN => "IN".to_string(),
            Class::CS => "CS".to_string(),
            Class::CH => "CH".to_string(),
            Class::HS => "HS".to_string(),
            Class::ALL => "*".to_string(),
        };
        write!(f, "{s}")?;
        Ok(())
    }
}
