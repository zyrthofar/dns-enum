use std::io::Cursor;

use bytes::Buf;
use data_encoding as enc;
use sha1::{Digest, Sha1};

use super::class::Class;
use super::io::ByteRead;
use super::name::Name;
use super::r#type::Type;
use super::resource_record::ResourceRecord;

#[derive(Clone, PartialEq, Debug)]
pub struct Record {
    pub name: Name,
    pub r#type: Type,
    pub class: Class,
    pub ttl: u32,
    pub alg: u8,
    pub flags: u8,
    pub its: u16,
    pub salt: Vec<u8>,
    pub hash: Vec<u8>,
    pub next_hash: Vec<u8>,
    pub types: Vec<Type>,
}

impl TryFrom<&ResourceRecord> for Record {
    type Error = String;

    /// # Errors
    ///
    /// Will return `Err` if the record cannot be added to the chain.
    #[allow(clippy::cast_possible_truncation)]
    fn try_from(rr: &ResourceRecord) -> Result<Self, String> {
        let labels = rr.name.labels();
        let hash_base32 = labels.first().ok_or("to have label".to_string())?;
        let hash = enc::BASE32HEX
            .decode(hash_base32.to_uppercase().as_bytes())
            .map_err(|_| "to decode label".to_string())?;

        let mut cursor = Cursor::new(rr.rdata.clone());

        let alg = cursor
            .read_u8()
            .map_err(|_| "error reading alg".to_string())?;
        let flags = cursor
            .read_u8()
            .map_err(|_| "error reading flags".to_string())?;
        let its = cursor
            .read_u16()
            .map_err(|_| "error reading its".to_string())?;

        let salt_length = cursor
            .read_u8()
            .map_err(|_| "error reading salt length".to_string())?
            as usize;
        let mut salt = vec![0; salt_length];
        cursor
            .read(&mut salt)
            .map_err(|_| "error reading salt".to_string())?;

        let next_hash_length = cursor
            .read_u8()
            .map_err(|_| "error reading next hash length".to_string())?
            as usize;
        let mut next_hash = vec![0; next_hash_length];
        cursor
            .read(&mut next_hash)
            .map_err(|_| "error reading next_hash".to_string())?;

        // Decodes the available types.
        let mut types = vec![];
        loop {
            if !cursor.has_remaining() {
                break;
            }

            let window_number = u16::from(
                cursor
                    .read_u8()
                    .map_err(|_| "error reading window number".to_string())?,
            );
            let bitmap_length = usize::from(
                cursor
                    .read_u8()
                    .map_err(|_| "error reading bitmap length".to_string())?,
            );
            let mut bitmap = vec![0; bitmap_length];
            cursor
                .read(&mut bitmap)
                .map_err(|_| "error reading bitmap".to_string())?;

            for (byte_index, byte) in bitmap.iter().enumerate() {
                for bit_index in 0..8 {
                    if byte >> (7 - bit_index) & 0x01 == 0 {
                        continue;
                    }

                    let type_id = (window_number << 8) + (byte_index << 3) as u16 + bit_index;
                    if let Ok(t) = Type::try_from(type_id) {
                        types.push(t);
                    }
                }
            }
        }

        let nsec3_record = Record {
            name: rr.name.clone(),
            r#type: rr.r#type,
            class: rr.class,
            ttl: rr.ttl,
            alg,
            flags,
            its,
            salt,
            hash,
            next_hash,
            types,
        };

        Ok(nsec3_record)
    }
}

pub struct Hasher {
    _algorithm: u8,
    iterations: u16,
    salt: Vec<u8>,
}

impl Hasher {
    /// # Errors
    ///
    /// Will return `Err` if the algorithm is invalid.
    pub fn new(algorithm: u8, iterations: u16, salt: Vec<u8>) -> Result<Self, String> {
        if algorithm != 1 {
            return Err("unimplemented algorithm".to_string());
        }

        Ok(Hasher {
            _algorithm: algorithm,
            iterations,
            salt,
        })
    }

    #[must_use]
    pub fn hash(&self, name: &Name) -> Vec<u8> {
        let mut hash = Vec::<u8>::from(name);
        for _ in 0..=self.iterations {
            hash.append(&mut self.salt.clone());
            hash = Sha1::digest(hash).to_vec();
        }

        hash
    }
}

#[derive(Debug)]
pub struct Chain {
    links: Vec<Option<Record>>,
}

impl Chain {
    #[must_use]
    pub fn new() -> Self {
        Chain { links: vec![None] }
    }

    #[must_use]
    pub fn len(&self) -> usize {
        self.links.iter().filter(|r| r.is_some()).count()
    }

    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.links.iter().all(Option::is_none)
    }

    /// # Errors
    ///
    /// Will return `Err` if the record cannot be added to the chain.
    #[allow(clippy::cast_possible_wrap)]
    pub fn add_record(&mut self, input: Record) -> Result<bool, String> {
        if self.is_empty() {
            if input.hash == input.next_hash {
                self.links.pop();
            }

            self.links.insert(0, Some(input));

            return Ok(true);
        } else if self
            .links
            .iter()
            .filter_map(|r| r.as_ref())
            .any(|r| r.hash == input.hash && r.next_hash == input.next_hash)
        {
            return Ok(false);
        } else if input.hash == input.next_hash {
            return Err("self-referencing record already present".to_string());
        }

        let i = self
            .links
            .iter()
            .position(|r| r.as_ref().is_some_and(|r| r.hash > input.hash))
            .unwrap_or(self.links.len());

        if let Some(previous_record) = self.nth_record(i as isize - 1) {
            if previous_record.hash == input.hash && previous_record.next_hash == input.next_hash {
                return Ok(false);
            }
            return Err("location is already taken".to_string());
        }

        if self
            .nth_record(i as isize)
            .as_ref()
            .is_some_and(|r| r.hash != input.next_hash)
        {
            self.links.insert(i, None);
        }

        let mut insert_index = i;
        if self
            .nth_record(i as isize - 2)
            .as_ref()
            .is_some_and(|r| r.next_hash == input.hash)
        {
            self.links.remove(self.wrap_index(i as isize - 1));
            if i > 0 {
                insert_index -= 1;
            }
        }

        self.links.insert(insert_index, Some(input));

        Ok(true)
    }

    #[must_use]
    pub fn has_record_with_hash(&self, hash: &Vec<u8>) -> bool {
        self.links
            .iter()
            .any(|r| r.as_ref().is_some_and(|r| &r.hash == hash))
    }

    #[allow(clippy::cast_possible_wrap)]
    #[must_use]
    pub fn is_accounted_for(&self, hash: &Vec<u8>) -> bool {
        if self
            .links
            .iter()
            .filter_map(|r| r.as_ref())
            .any(|r| &r.hash <= hash && hash < &r.next_hash)
        {
            return true;
        }

        let i = self
            .links
            .iter()
            .position(|r| r.as_ref().is_some_and(|r| &r.hash > hash))
            .unwrap_or(self.links.len());

        self.nth_record(i as isize - 1).is_some()
    }

    #[must_use]
    pub fn is_complete(&self) -> bool {
        !self.links.iter().any(Option::is_none)
    }

    #[must_use]
    pub fn partial_records(&self) -> Vec<Option<Record>> {
        self.links.clone()
    }

    /// # Panics
    ///
    /// Will panic if the chain is incomplete.
    #[must_use]
    pub fn records(&self) -> Vec<Record> {
        assert!(
            self.is_complete(),
            "chain is incomplete, use partial_records instead"
        );

        self.links.iter().map(|r| r.clone().unwrap()).collect()
    }

    fn nth_record(&self, i: isize) -> &Option<Record> {
        self.links.get(self.wrap_index(i)).unwrap()
    }

    #[allow(clippy::cast_possible_wrap)]
    #[allow(clippy::cast_sign_loss)]
    fn wrap_index(&self, index: isize) -> usize {
        let len = self.links.len() as isize;

        let mut i = index;
        while i < 0 {
            i += len;
        }
        while i >= len {
            i -= len;
        }

        i as usize
    }
}

impl Default for Chain {
    fn default() -> Self {
        Self::new()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    fn build_record(hash: u8, next_hash: u8) -> Record {
        Record {
            name: Name::new(""),
            r#type: Type::NSEC3,
            class: Class::IN,
            ttl: 0,
            alg: 1,
            flags: 0,
            its: 0,
            salt: vec![],
            hash: vec![hash],
            next_hash: vec![next_hash],
            types: vec![],
        }
    }

    #[test]
    fn test_new() {
        let c = Chain::new();
        assert_eq!(c.partial_records(), vec![None]);
    }

    #[test]
    fn test_add_record_to_empty_chain() {
        let mut c = Chain::new();

        // () + (45) --> (45)()
        let r = c.add_record(build_record(4, 5));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(c.partial_records(), vec![Some(build_record(4, 5)), None,],);
    }

    #[test]
    fn test_add_self_referencing_record_to_empty_chain() {
        let mut c = Chain::new();

        // () + (55) --> (55)
        let r = c.add_record(build_record(5, 5));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(c.partial_records(), vec![Some(build_record(5, 5)),],);
    }

    #[test]
    fn test_add_matching_record_to_start_of_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(4, 5));

        // (45)() + (34) --> (34)(45)()
        let r = c.add_record(build_record(3, 4));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![Some(build_record(3, 4)), Some(build_record(4, 5)), None,],
        );
    }

    #[test]
    fn test_add_matching_record_to_middle_of_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(2, 3));
        _ = c.add_record(build_record(4, 5));

        // (23)()(45)() + (34) --> (23)(34)(45)()
        let r = c.add_record(build_record(3, 4));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(2, 3)),
                Some(build_record(3, 4)),
                Some(build_record(4, 5)),
                None,
            ],
        );
    }

    #[test]
    fn test_add_matching_record_to_end_of_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(4, 5));

        // (45)() + (56) --> (45)(56)()
        let r = c.add_record(build_record(5, 6));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![Some(build_record(4, 5)), Some(build_record(5, 6)), None,],
        );
    }

    #[test]
    fn test_add_non_matching_record_to_start_of_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(4, 5));

        // (45)() + (23) --> (23)()(45)()
        let r = c.add_record(build_record(2, 3));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(2, 3)),
                None,
                Some(build_record(4, 5)),
                None,
            ],
        );
    }

    #[test]
    fn test_add_non_matching_record_to_middle_of_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(2, 3));
        _ = c.add_record(build_record(6, 7));

        // (23)()(67)() + (45) --> (23)()(45)()(67)()
        let r = c.add_record(build_record(4, 5));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(2, 3)),
                None,
                Some(build_record(4, 5)),
                None,
                Some(build_record(6, 7)),
                None,
            ],
        );
    }

    #[test]
    fn test_add_non_matching_record_to_end_of_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(4, 5));

        // (45)() + (67) --> (45)()(67)()
        let r = c.add_record(build_record(6, 7));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(4, 5)),
                None,
                Some(build_record(6, 7)),
                None,
            ],
        );
    }

    #[test]
    fn test_add_wrapping_record_to_empty_chain() {
        let mut c = Chain::new();

        // () + (54) --> (54)()
        let r = c.add_record(build_record(5, 4));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(c.partial_records(), vec![Some(build_record(5, 4)), None,],);
    }

    #[test]
    fn test_add_wrapping_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(4, 5));

        // (45)() + (62) --> (45)()(62)()
        let r = c.add_record(build_record(6, 2));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(4, 5)),
                None,
                Some(build_record(6, 2)),
                None,
            ],
        );
    }

    #[test]
    fn test_add_wrapping_record_matching_previous_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(4, 5));

        // (34)(45)() + (52) --> (34)(45)(52)()
        let r = c.add_record(build_record(5, 2));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(3, 4)),
                Some(build_record(4, 5)),
                Some(build_record(5, 2)),
                None,
            ],
        );
    }

    #[test]
    fn test_add_wrapping_record_matching_next_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(4, 5));

        // (34)(45)() + (63) --> (34)(45)()(63)
        let r = c.add_record(build_record(6, 3));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(3, 4)),
                Some(build_record(4, 5)),
                None,
                Some(build_record(6, 3)),
            ],
        );
    }

    #[test]
    fn test_add_wrapping_record_matching_sibling_records_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(4, 5));

        // (34)(45)() + (53) --> (34)(45)(53)
        let r = c.add_record(build_record(5, 3));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(3, 4)),
                Some(build_record(4, 5)),
                Some(build_record(5, 3)),
            ],
        );
    }

    #[test]
    fn test_add_record_matching_previous_wrapping_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(4, 5));
        _ = c.add_record(build_record(5, 2));

        // (45)(52)() + (23) --> (23)()(45)(52)
        let r = c.add_record(build_record(2, 3));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(2, 3)),
                None,
                Some(build_record(4, 5)),
                Some(build_record(5, 2)),
            ],
        );
    }

    #[test]
    fn test_add_record_matching_next_wrapping_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(6, 2));

        // (34)()(62)() + (56) --> (34)()(56)(62)()
        let r = c.add_record(build_record(5, 6));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(3, 4)),
                None,
                Some(build_record(5, 6)),
                Some(build_record(6, 2)),
                None,
            ],
        );
    }

    #[test]
    fn test_add_record_matching_sibling_wrapping_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(5, 4));

        // (54)() + (45) --> (45)(54)
        let r = c.add_record(build_record(4, 5));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(true));

        assert_eq!(
            c.partial_records(),
            vec![Some(build_record(4, 5)), Some(build_record(5, 4)),],
        );
    }

    #[test]
    fn test_add_record_to_already_filled_spot_in_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 6));
        _ = c.add_record(build_record(6, 7));

        // (36)(67)() + (45) --> error
        let r = c.add_record(build_record(4, 5));
        assert_eq!(r.err(), Some("location is already taken".to_string()));

        assert_eq!(
            c.partial_records(),
            vec![Some(build_record(3, 6)), Some(build_record(6, 7)), None,],
        );
    }

    #[test]
    fn test_add_existing_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(4, 5));
        _ = c.add_record(build_record(5, 6));

        let r = c.add_record(build_record(3, 4));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(false));

        let r = c.add_record(build_record(4, 5));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(false));

        let r = c.add_record(build_record(5, 6));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(false));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(3, 4)),
                Some(build_record(4, 5)),
                Some(build_record(5, 6)),
                None,
            ],
        );
    }

    #[test]
    fn test_add_existing_self_referencing_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(5, 5));

        // (55) + (55) --> (55)
        let r = c.add_record(build_record(5, 5));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(false));

        assert_eq!(c.partial_records(), vec![Some(build_record(5, 5)),],);
    }

    #[test]
    fn test_add_existing_self_referencing_record_to_non_empty_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(4, 5));

        // (45)() + (55) --> error
        let r = c.add_record(build_record(5, 5));
        assert_eq!(
            r.err(),
            Some("self-referencing record already present".to_string())
        );

        assert_eq!(c.partial_records(), vec![Some(build_record(4, 5)), None,],);
    }

    #[test]
    fn test_add_existing_wrapping_record_to_chain() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(4, 5));
        _ = c.add_record(build_record(5, 3));

        let r = c.add_record(build_record(5, 3));
        assert_eq!(r.as_ref().err(), None);
        assert_eq!(r.ok(), Some(false));

        assert_eq!(
            c.partial_records(),
            vec![
                Some(build_record(3, 4)),
                Some(build_record(4, 5)),
                Some(build_record(5, 3)),
            ],
        );
    }

    #[test]
    fn test_has_record_with_hash() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 5));

        assert!(!c.has_record_with_hash(&vec![2]));
        assert!(c.has_record_with_hash(&vec![3]));
        assert!(!c.has_record_with_hash(&vec![4]));
        assert!(!c.has_record_with_hash(&vec![5]));
        assert!(!c.has_record_with_hash(&vec![6]));
    }

    #[test]
    fn test_is_missing() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(4, 6));

        assert!(!c.is_accounted_for(&vec![2]));
        assert!(c.is_accounted_for(&vec![3]));
        assert!(c.is_accounted_for(&vec![4]));
        assert!(c.is_accounted_for(&vec![5]));
        assert!(!c.is_accounted_for(&vec![6]));
        assert!(!c.is_accounted_for(&vec![7]));
    }

    #[test]
    fn test_is_complete() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(4, 5));

        assert!(!c.is_complete());

        _ = c.add_record(build_record(5, 3));
        assert!(c.is_complete());
    }

    #[test]
    fn test_partial_records() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(5, 3));
        _ = c.add_record(build_record(3, 4));

        assert_eq!(
            c.partial_records(),
            vec![Some(build_record(3, 4)), None, Some(build_record(5, 3)),],
        );
    }

    #[test]
    fn test_records() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(5, 3));
        _ = c.add_record(build_record(3, 4));
        _ = c.add_record(build_record(4, 5));

        let records = c.records();
        assert_eq!(
            records,
            vec![build_record(3, 4), build_record(4, 5), build_record(5, 3),],
        );
    }

    #[test]
    #[should_panic(expected = "chain is incomplete, use partial_records instead")]
    fn test_records_with_missing_links() {
        let mut c = Chain::new();
        _ = c.add_record(build_record(5, 3));
        _ = c.add_record(build_record(3, 4));

        _ = c.records();
    }
}
