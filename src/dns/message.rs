use std::io::Cursor;

use super::class::Class;
use super::io::{ByteRead, ByteWrite};
use super::name::Name;
use super::opcode::Opcode;
use super::question::Question;
use super::r#type::Type;
use super::rcode::RCode;
use super::resource_record::ResourceRecord;

// 4.1.1. Header section format
//
// The header contains the following fields:
//
//                                     1  1  1  1  1  1
//       0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                      ID                       |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |QR|   Opcode  |AA|TC|RD|RA|   Z    |   RCODE   |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                    QDCOUNT                    |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                    ANCOUNT                    |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                    NSCOUNT                    |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                    ARCOUNT                    |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//
// where:
//
// ID              A 16 bit identifier assigned by the program that
//                 generates any kind of query.  This identifier is copied
//                 the corresponding reply and can be used by the requester
//                 to match up replies to outstanding queries.
//
// QR              A one bit field that specifies whether this message is a
//                 query (0), or a response (1).
//
// OPCODE          A four bit field that specifies kind of query in this
//                 message.  This value is set by the originator of a query
//                 and copied into the response.  The values are:
//
//                 0               a standard query (QUERY)
//
//                 1               an inverse query (IQUERY)
//
//                 2               a server status request (STATUS)
//
//                 3-15            reserved for future use
//
// AA              Authoritative Answer - this bit is valid in responses,
//                 and specifies that the responding name server is an
//                 authority for the domain name in question section.
//
//                 Note that the contents of the answer section may have
//                 multiple owner names because of aliases.  The AA bit
//                 corresponds to the name which matches the query name, or
//                 the first owner name in the answer section.
//
// TC              TrunCation - specifies that this message was truncated
//                 due to length greater than that permitted on the
//                 transmission channel.
//
// RD              Recursion Desired - this bit may be set in a query and
//                 is copied into the response.  If RD is set, it directs
//                 the name server to pursue the query recursively.
//                 Recursive query support is optional.
//
// RA              Recursion Available - this be is set or cleared in a
//                 response, and denotes whether recursive query support is
//                 available in the name server.
//
// Z               Reserved for future use.  Must be zero in all queries
//                 and responses.
//
// RCODE           Response code - this 4 bit field is set as part of
//                 responses.  The values have the following
//                 interpretation:
//
//                 0               No error condition
//
//                 1               Format error - The name server was
//                                 unable to interpret the query.
//
//                 2               Server failure - The name server was
//                                 unable to process this query due to a
//                                 problem with the name server.
//
//                 3               Name Error - Meaningful only for
//                                 responses from an authoritative name
//                                 server, this code signifies that the
//                                 domain name referenced in the query does
//                                 not exist.
//
//                 4               Not Implemented - The name server does
//                                 not support the requested kind of query.
//
//                 5               Refused - The name server refuses to
//                                 perform the specified operation for
//                                 policy reasons.  For example, a name
//                                 server may not wish to provide the
//                                 information to the particular requester,
//                                 or a name server may not wish to perform
//                                 a particular operation (e.g., zone
//                                 transfer) for particular data.
//
//                 6-15            Reserved for future use.
//
// QDCOUNT         an unsigned 16 bit integer specifying the number of
//                 entries in the question section.
//
// ANCOUNT         an unsigned 16 bit integer specifying the number of
//                 resource records in the answer section.
//
// NSCOUNT         an unsigned 16 bit integer specifying the number of name
//                 server resource records in the authority records
//                 section.
//
// ARCOUNT         an unsigned 16 bit integer specifying the number of
//                 resource records in the additional records section.

#[allow(clippy::struct_excessive_bools)]
#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Message {
    pub id: u16,
    pub qr: bool,
    pub opcode: Opcode,
    pub aa: bool,
    pub tc: bool,
    pub rd: bool,
    pub ra: bool,
    pub rcode: RCode,
    pub questions: Vec<Question>,
    pub answers: Vec<ResourceRecord>,
    pub authorities: Vec<ResourceRecord>,
    pub additionals: Vec<ResourceRecord>,
}

impl Message {
    // pub fn new(
    //     id: u16,
    //     qr: bool,
    //     opcode: Opcode,
    //     aa: bool,
    //     tc: bool,
    //     rd: bool,
    //     ra: bool,
    //     rcode: RCode,
    //     questions: Vec<Question>,
    //     answers: Vec<ResourceRecord>,
    //     authorities: Vec<ResourceRecord>,
    //     additionals: Vec<ResourceRecord>,
    // ) -> Self {
    //     Message {
    //         id,
    //         qr,
    //         opcode,
    //         aa,
    //         tc,
    //         rd,
    //         ra,
    //         rcode,
    //         questions,
    //         answers,
    //         authorities,
    //         additionals,
    //     }
    // }

    #[must_use]
    pub fn new_query(qname: Name, qtype: Type, qclass: Class) -> Self {
        let question = Question {
            qname,
            qtype,
            qclass,
        };

        Message {
            id: rand::random::<u16>(),
            qr: false,
            opcode: Opcode::Query,
            aa: false,
            tc: false,
            rd: true,
            ra: false,
            rcode: RCode::NoError,
            questions: vec![question],
            answers: vec![],
            authorities: vec![],
            additionals: vec![],
        }
    }
}

impl TryFrom<Vec<u8>> for Message {
    type Error = String;

    #[allow(clippy::similar_names)]
    fn try_from(v: Vec<u8>) -> Result<Self, String> {
        let mut cursor = Cursor::new(v.clone());

        let id = cursor.read_u16()?;
        let options = cursor.read_u16()?;
        let qdcount = cursor.read_u16()?;
        let ancount = cursor.read_u16()?;
        let nscount = cursor.read_u16()?;
        let arcount = cursor.read_u16()?;

        let mut msg = Message {
            id,
            qr: options & 0b_1000_0000_0000_0000 != 0,
            opcode: Opcode::try_from((options & 0b_0111_1000_0000_0000) >> 11)?,
            aa: options & 0b_0000_0100_0000_0000 != 0,
            tc: options & 0b_0000_0010_0000_0000 != 0,
            rd: options & 0b_0000_0001_0000_0000 != 0,
            ra: options & 0b_0000_0000_1000_0000 != 0,
            rcode: RCode::try_from(options & 0b_0000_0000_0000_1111)?,
            questions: Vec::new(),
            answers: Vec::new(),
            authorities: Vec::new(),
            additionals: Vec::new(),
        };

        for _ in 0..qdcount {
            let q = parse_question(&mut cursor)?;
            msg.questions.push(q);
        }

        for _ in 0..ancount {
            let rr = parse_resource_record(&mut cursor)?;
            msg.answers.push(rr);
        }

        for _ in 0..nscount {
            let rr = parse_resource_record(&mut cursor)?;
            msg.authorities.push(rr);
        }

        for _ in 0..arcount {
            let rr = parse_resource_record(&mut cursor)?;
            msg.additionals.push(rr);
        }

        Ok(msg)
    }
}

impl std::fmt::Display for Message {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[{}] {},", self.id, self.opcode)?;

        if self.qr {
            write!(f, " QR")?;
        }
        if self.aa {
            write!(f, " AA")?;
        }
        if self.tc {
            write!(f, " TC")?;
        }
        if self.rd {
            write!(f, " RD")?;
        }
        if self.ra {
            write!(f, " RA")?;
        }

        write!(f, ", {}", self.rcode)?;
        write!(f, ", {} QD", self.questions.len())?;
        write!(f, ", {} AN", self.answers.len())?;
        write!(f, ", {} NS", self.authorities.len())?;
        write!(f, ", {} AR", self.additionals.len())?;
        writeln!(f)?;

        for q in &self.questions {
            writeln!(f, "QD {q}")?;
        }

        for rr in &self.answers {
            writeln!(f, "AN {rr}")?;
        }

        for rr in &self.authorities {
            writeln!(f, "NS {rr}")?;
        }

        for rr in &self.additionals {
            writeln!(f, "AR {rr}")?;
        }

        Ok(())
    }
}

impl From<Message> for Vec<u8> {
    #[allow(clippy::cast_possible_truncation)]
    fn from(v: Message) -> Self {
        let mut bytes: Vec<u8> = Vec::new();

        let mut options = 0_u16;
        if v.qr {
            options |= 0b_1000_0000_0000_0000;
        }
        options |= u16::from(v.opcode) << 11;
        if v.aa {
            options |= 0b_0000_0100_0000_0000;
        }
        if v.tc {
            options |= 0b_0000_0010_0000_0000;
        }
        if v.rd {
            options |= 0b_0000_0001_0000_0000;
        }
        if v.ra {
            options |= 0b_0000_0000_1000_0000;
        }
        options |= u16::from(v.rcode);

        bytes.write_u16(v.id);
        bytes.write_u16(options);
        bytes.write_u16(v.questions.len() as u16);
        bytes.write_u16(v.answers.len() as u16);
        bytes.write_u16(v.authorities.len() as u16);
        bytes.write_u16(v.additionals.len() as u16);

        for q in &v.questions {
            bytes.write(Vec::<u8>::from(q).as_slice());
        }

        for rr in &v.answers {
            bytes.write(Vec::<u8>::from(rr).as_slice());
        }

        for rr in &v.authorities {
            bytes.write(Vec::<u8>::from(rr).as_slice());
        }

        for rr in &v.additionals {
            bytes.write(Vec::<u8>::from(rr).as_slice());
        }

        bytes
    }
}

fn parse_question(bytes: &mut Cursor<Vec<u8>>) -> Result<Question, String> {
    Ok(Question {
        qname: parse_name(bytes)?,
        qtype: Type::try_from(bytes.read_u16()?)?,
        qclass: Class::try_from(bytes.read_u16()?)?,
    })
}

fn parse_resource_record(bytes: &mut Cursor<Vec<u8>>) -> Result<ResourceRecord, String> {
    let name = parse_name(bytes)?;
    let r#type = Type::try_from(bytes.read_u16()?)?;

    let class: Class;
    let ttl: u32;
    let mut rdata: Vec<u8>;

    if r#type == Type::OPT {
        // Fakes OPT pseudo-records into a normal record.
        class = Class::IN;
        ttl = 0;

        let current_position = bytes.position();
        bytes.set_position(current_position + 6);
        let rdlength = 8 + (bytes.read_u16()? as usize);
        bytes.set_position(current_position);
        rdata = vec![0; rdlength];
        bytes.read(&mut rdata)?;
    } else {
        class = Class::try_from(bytes.read_u16()?)?;
        ttl = bytes.read_u32()?;

        let rdlength = bytes.read_u16()? as usize;
        rdata = vec![0; rdlength];
        bytes.read(&mut rdata)?;
    }

    let rr = ResourceRecord {
        name,
        r#type,
        class,
        ttl,
        rdata,
    };

    Ok(rr)
}

fn parse_name(bytes: &mut Cursor<Vec<u8>>) -> Result<Name, String> {
    let mut fqdn = String::new();

    loop {
        // if !v.has_remaining() { return Err(Error::Deserialization) }

        let len = bytes.read_u8()? as usize;
        if len == 0 {
            break;
        }

        // Name compression
        if len & 0b_1100_0000 == 0b_1100_0000 {
            let mut offset = bytes.read_u8()? as usize;
            offset += (len & 0b_0011_1111) << 8;

            let current_pos = bytes.position();
            bytes.set_position(offset as u64);

            let name = parse_name(bytes)?;
            fqdn.push_str(name.to_string().as_ref());

            bytes.set_position(current_pos);

            return Ok(Name::from(fqdn));
        }

        let mut label = vec![0; len];
        bytes.read(&mut label)?;
        fqdn.push_str(std::str::from_utf8(&label).unwrap());
        fqdn.push('.');
    }

    Ok(Name::from(fqdn))
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new_query() {
        let message = Message::new_query(Name::new("example.com."), Type::A, Class::IN);

        assert!(!message.qr);
        assert_eq!(message.opcode, Opcode::Query);
        assert!(!message.aa);
        assert!(!message.tc);
        assert!(message.rd);
        assert!(!message.ra);
        assert_eq!(message.rcode, RCode::NoError);

        assert_eq!(message.questions.len(), 1);
        assert_eq!(
            message.questions[0],
            Question {
                qname: Name::new("example.com."),
                qtype: Type::A,
                qclass: Class::IN,
            },
        );

        assert_eq!(message.answers.len(), 0);
        assert_eq!(message.authorities.len(), 0);
        assert_eq!(message.additionals.len(), 0);
    }

    #[test]
    fn test_try_from_vec_u8() {
        let bytes = [
            b"\x01\x23\x97\x83\x00\x01\x00\x01\x00\x01\x00\x01".as_slice(),
            b"\x01a\x07example\x03com\x00\x00\x01\x00\x01".as_slice(),
            b"\x01b\x07example\x03com\x00\x00\x02\x00\x01\x00\x00\x0e\x10\x00\x04\x01\x23\x45\x67"
                .as_slice(),
            b"\x01c\x07example\x03com\x00\x00\x03\x00\x01\x00\x00\x0e\x11\x00\x04\x01\x23\x45\x68"
                .as_slice(),
            b"\x01d\x07example\x03com\x00\x00\x04\x00\x01\x00\x00\x0e\x12\x00\x04\x01\x23\x45\x69"
                .as_slice(),
        ]
        .concat()
        .to_vec();

        let result = Message::try_from(bytes);
        assert!(result.is_ok());

        let message = result.unwrap();
        assert_eq!(message.id, 0x0123);
        assert!(message.qr);
        assert_eq!(message.opcode, Opcode::Status);
        assert!(message.aa);
        assert!(message.tc);
        assert!(message.rd);
        assert!(message.ra);
        assert_eq!(message.rcode, RCode::NameError);

        assert_eq!(message.questions.len(), 1);
        assert_eq!(
            message.questions[0],
            Question {
                qname: Name::new("a.example.com."),
                qtype: Type::A,
                qclass: Class::IN,
            },
        );

        assert_eq!(message.answers.len(), 1);
        assert_eq!(
            message.answers[0],
            ResourceRecord {
                name: Name::new("b.example.com."),
                r#type: Type::NS,
                class: Class::IN,
                ttl: 3600,
                rdata: vec![0x01, 0x23, 0x45, 0x67],
            },
        );

        assert_eq!(message.authorities.len(), 1);
        assert_eq!(
            message.authorities[0],
            ResourceRecord {
                name: Name::new("c.example.com."),
                r#type: Type::MD,
                class: Class::IN,
                ttl: 3601,
                rdata: vec![0x01, 0x23, 0x45, 0x68],
            },
        );

        assert_eq!(message.additionals.len(), 1);
        assert_eq!(
            message.additionals[0],
            ResourceRecord {
                name: Name::new("d.example.com."),
                r#type: Type::MF,
                class: Class::IN,
                ttl: 3602,
                rdata: vec![0x01, 0x23, 0x45, 0x69],
            },
        );
    }

    #[test]
    fn test_display() {
        let message = Message {
            id: 0x0123,
            qr: true,
            opcode: Opcode::Query,
            aa: false,
            tc: false,
            rd: true,
            ra: true,
            rcode: RCode::NoError,
            questions: vec![Question {
                qname: Name::new("example.com."),
                qtype: Type::A,
                qclass: Class::IN,
            }],
            answers: vec![ResourceRecord {
                name: Name::new("example.com."),
                r#type: Type::A,
                class: Class::IN,
                ttl: 3600,
                rdata: vec![0x5d, 0xb8, 0xd8, 0x22],
            }],
            authorities: vec![],
            additionals: vec![],
        };

        assert_eq!(
            message.to_string(),
            "[291] QUERY, QR RD RA, NoError, 1 QD, 1 AN, 0 NS, 0 AR\nQD example.com. IN A\nAN example.com. IN A, ttl 3600s\n".to_string(),
        );
    }

    #[test]
    fn test_into_vec_u8() {
        let message = Message {
            id: 0x0123,
            qr: true,
            opcode: Opcode::Status,
            aa: true,
            tc: true,
            rd: true,
            ra: true,
            rcode: RCode::NameError,
            questions: vec![Question {
                qname: Name::new("a.example.com."),
                qtype: Type::A,
                qclass: Class::IN,
            }],
            answers: vec![ResourceRecord {
                name: Name::new("b.example.com."),
                r#type: Type::NS,
                class: Class::IN,
                ttl: 3600,
                rdata: vec![0x01, 0x23, 0x45, 0x67],
            }],
            authorities: vec![ResourceRecord {
                name: Name::new("c.example.com."),
                r#type: Type::MD,
                class: Class::IN,
                ttl: 3601,
                rdata: vec![0x01, 0x23, 0x45, 0x68],
            }],
            additionals: vec![ResourceRecord {
                name: Name::new("d.example.com."),
                r#type: Type::MF,
                class: Class::IN,
                ttl: 3602,
                rdata: vec![0x01, 0x23, 0x45, 0x69],
            }],
        };

        assert_eq!(
            Vec::<u8>::from(message),
            [
                b"\x01\x23\x97\x83\x00\x01\x00\x01\x00\x01\x00\x01".as_slice(),
                b"\x01a\x07example\x03com\x00\x00\x01\x00\x01".as_slice(),
                b"\x01b\x07example\x03com\x00\x00\x02\x00\x01\x00\x00\x0e\x10\x00\x04\x01\x23\x45\x67".as_slice(),
                b"\x01c\x07example\x03com\x00\x00\x03\x00\x01\x00\x00\x0e\x11\x00\x04\x01\x23\x45\x68".as_slice(),
                b"\x01d\x07example\x03com\x00\x00\x04\x00\x01\x00\x00\x0e\x12\x00\x04\x01\x23\x45\x69".as_slice(),
            ].concat().to_vec(),
        );
    }

    #[test]
    fn test_from_example_com_a_query() {
        // Query sent by `dig @a.iana-servers.net. example.com. A`
        // 00000000: 0123 0120 0001 0000 0000 0001 0765 7861  .#. .........exa
        // 00000010: 6d70 6c65 0363 6f6d 0000 0100 0100 0029  mple.com.......)
        // 00000020: 1000 0000 0000 000c 000a 0008 0123 4567  .............#Eg
        // 00000030: 89ab cdef                                ....

        let bytes = [
            b"\x01\x23\x01\x20\x00\x01\x00\x00\x00\x00\x00\x01\x07\x65\x78\x61".as_slice(),
            b"\x6d\x70\x6c\x65\x03\x63\x6f\x6d\x00\x00\x01\x00\x01\x00\x00\x29".as_slice(),
            b"\x10\x00\x00\x00\x00\x00\x00\x0c\x00\x0a\x00\x08\x01\x23\x45\x67".as_slice(),
            b"\x89\xab\xcd\xef".as_slice(),
        ]
        .concat()
        .to_vec();

        assert_eq!(
            Message::try_from(bytes),
            Ok(Message {
                id: 0x0123,
                qr: false,
                opcode: Opcode::Query,
                aa: false,
                tc: false,
                rd: true,
                ra: false,
                rcode: RCode::NoError,
                questions: vec![Question {
                    qname: Name::new("example.com."),
                    qtype: Type::A,
                    qclass: Class::IN,
                },],
                answers: vec![],
                authorities: vec![],
                additionals: vec![ResourceRecord {
                    name: Name::new(""),
                    r#type: Type::OPT,
                    class: Class::IN,
                    ttl: 0,
                    rdata: vec![
                        0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x0a, 0x00, 0x08,
                        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
                    ],
                },],
            }),
        );
    }

    #[test]
    fn test_from_example_com_a_response() {
        // Response received from `dig @a.iana-servers.net. example.com. A`
        // 00000000: 0123 8500 0001 0001 0000 0001 0765 7861  .#...........exa
        // 00000010: 6d70 6c65 0363 6f6d 0000 0100 01c0 0c00  mple.com........
        // 00000020: 0100 0100 0151 8000 045d b8d8 2200 0029  .....Q...].."..)
        // 00000030: 1000 0000 0000 0000                      ........

        let bytes = [
            b"\x01\x23\x85\x00\x00\x01\x00\x01\x00\x00\x00\x01\x07\x65\x78\x61".as_slice(),
            b"\x6d\x70\x6c\x65\x03\x63\x6f\x6d\x00\x00\x01\x00\x01\xc0\x0c\x00".as_slice(),
            b"\x01\x00\x01\x00\x01\x51\x80\x00\x04\x5d\xb8\xd8\x22\x00\x00\x29".as_slice(),
            b"\x10\x00\x00\x00\x00\x00\x00\x00".as_slice(),
        ]
        .concat()
        .to_vec();

        assert_eq!(
            Message::try_from(bytes),
            Ok(Message {
                id: 0x0123,
                qr: true,
                opcode: Opcode::Query,
                aa: true,
                tc: false,
                rd: true,
                ra: false,
                rcode: RCode::NoError,
                questions: vec![Question {
                    qname: Name::new("example.com."),
                    qtype: Type::A,
                    qclass: Class::IN,
                },],
                answers: vec![ResourceRecord {
                    name: Name::new("example.com."),
                    r#type: Type::A,
                    class: Class::IN,
                    ttl: 86400,
                    rdata: vec![0x5d, 0xb8, 0xd8, 0x22],
                },],
                authorities: vec![],
                additionals: vec![ResourceRecord {
                    name: Name::new(""),
                    r#type: Type::OPT,
                    class: Class::IN,
                    ttl: 0,
                    rdata: vec![0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
                },],
            }),
        );
    }

    #[test]
    fn test_from_example_com_ns_query() {
        // Query sent by `dig @a.iana-servers.net. example.com. NS`
        // 00000000: 0123 0120 0001 0000 0000 0001 0765 7861  .#. .........exa
        // 00000010: 6d70 6c65 0363 6f6d 0000 0200 0100 0029  mple.com.......)
        // 00000020: 1000 0000 0000 000c 000a 0008 0123 4567  .............#Eg
        // 00000030: 89ab cdef                                ....

        let bytes = [
            b"\x01\x23\x01\x20\x00\x01\x00\x00\x00\x00\x00\x01\x07\x65\x78\x61".as_slice(),
            b"\x6d\x70\x6c\x65\x03\x63\x6f\x6d\x00\x00\x02\x00\x01\x00\x00\x29".as_slice(),
            b"\x10\x00\x00\x00\x00\x00\x00\x0c\x00\x0a\x00\x08\x01\x23\x45\x67".as_slice(),
            b"\x89\xab\xcd\xef".as_slice(),
        ]
        .concat()
        .to_vec();

        assert_eq!(
            Message::try_from(bytes),
            Ok(Message {
                id: 0x0123,
                qr: false,
                opcode: Opcode::Query,
                aa: false,
                tc: false,
                rd: true,
                ra: false,
                rcode: RCode::NoError,
                questions: vec![Question {
                    qname: Name::new("example.com."),
                    qtype: Type::NS,
                    qclass: Class::IN,
                },],
                answers: vec![],
                authorities: vec![],
                additionals: vec![ResourceRecord {
                    name: Name::new(""),
                    r#type: Type::OPT,
                    class: Class::IN,
                    ttl: 0,
                    rdata: vec![
                        0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x0c, 0x00, 0x0a, 0x00, 0x08,
                        0x01, 0x23, 0x45, 0x67, 0x89, 0xab, 0xcd, 0xef,
                    ],
                },],
            }),
        );
    }

    #[test]
    fn test_from_example_com_ns_response() {
        // Response received from `dig @a.iana-servers.net. example.com. NS`
        // 00000000: 0123 8500 0001 0002 0000 0001 0765 7861  .#...........exa
        // 00000010: 6d70 6c65 0363 6f6d 0000 0200 01c0 0c00  mple.com........
        // 00000020: 0200 0100 0151 8000 1401 610c 6961 6e61  .....Q....a.iana
        // 00000030: 2d73 6572 7665 7273 036e 6574 00c0 0c00  -servers.net....
        // 00000040: 0200 0100 0151 8000 0401 62c0 2b00 0029  .....Q....b.+..)
        // 00000050: 1000 0000 0000 0000                      ........

        let bytes = [
            b"\x01\x23\x85\x00\x00\x01\x00\x02\x00\x00\x00\x01\x07\x65\x78\x61".as_slice(),
            b"\x6d\x70\x6c\x65\x03\x63\x6f\x6d\x00\x00\x02\x00\x01\xc0\x0c\x00".as_slice(),
            b"\x02\x00\x01\x00\x01\x51\x80\x00\x14\x01\x61\x0c\x69\x61\x6e\x61".as_slice(),
            b"\x2d\x73\x65\x72\x76\x65\x72\x73\x03\x6e\x65\x74\x00\xc0\x0c\x00".as_slice(),
            b"\x02\x00\x01\x00\x01\x51\x80\x00\x04\x01\x62\xc0\x2b\x00\x00\x29".as_slice(),
            b"\x10\x00\x00\x00\x00\x00\x00\x00".as_slice(),
        ]
        .concat()
        .to_vec();

        assert_eq!(
            Message::try_from(bytes),
            Ok(Message {
                id: 0x0123,
                qr: true,
                opcode: Opcode::Query,
                aa: true,
                tc: false,
                rd: true,
                ra: false,
                rcode: RCode::NoError,
                questions: vec![Question {
                    qname: Name::new("example.com."),
                    qtype: Type::NS,
                    qclass: Class::IN,
                },],
                answers: vec![
                    ResourceRecord {
                        name: Name::new("example.com."),
                        r#type: Type::NS,
                        class: Class::IN,
                        ttl: 86400,
                        rdata: vec![
                            0x01, 0x61, 0x0c, 0x69, 0x61, 0x6e, 0x61, 0x2d, 0x73, 0x65, 0x72, 0x76,
                            0x65, 0x72, 0x73, 0x03, 0x6e, 0x65, 0x74, 0x00,
                        ],
                    },
                    ResourceRecord {
                        name: Name::new("example.com."),
                        r#type: Type::NS,
                        class: Class::IN,
                        ttl: 86400,
                        rdata: vec![0x01, 0x62, 0xc0, 0x2b],
                    },
                ],
                authorities: vec![],
                additionals: vec![ResourceRecord {
                    name: Name::new(""),
                    r#type: Type::OPT,
                    class: Class::IN,
                    ttl: 0,
                    rdata: vec![0x10, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00],
                },],
            }),
        );
    }
}
