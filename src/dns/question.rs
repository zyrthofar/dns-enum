// use bytes::{Bytes, BytesMut, BufMut};

use super::class::Class;
use super::io::ByteWrite;
use super::name::Name;
use super::r#type::Type;

// 4.1.2. Question section format
//
// The question section is used to carry the "question" in most queries,
// i.e., the parameters that define what is being asked.  The section
// contains QDCOUNT (usually 1) entries, each of the following format:
//
//                                     1  1  1  1  1  1
//       0  1  2  3  4  5  6  7  8  9  0  1  2  3  4  5
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                                               |
//     /                     QNAME                     /
//     /                                               /
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                     QTYPE                     |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//     |                     QCLASS                    |
//     +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
//
// where:
//
// QNAME           a domain name represented as a sequence of labels, where
//                 each label consists of a length octet followed by that
//                 number of octets.  The domain name terminates with the
//                 zero length octet for the null label of the root.  Note
//                 that this field may be an odd number of octets; no
//                 padding is used.
//
// QTYPE           a two octet code which specifies the type of the query.
//                 The values for this field include all codes valid for a
//                 TYPE field, together with some more general codes which
//                 can match more than one type of RR.
//
// QCLASS          a two octet code that specifies the class of the query.
//                 For example, the QCLASS field is IN for the Internet.

#[derive(Clone, Eq, PartialEq, Debug)]
pub struct Question {
    pub qname: Name,
    pub qtype: Type,
    pub qclass: Class,
}

impl std::fmt::Display for Question {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.qname)?;
        write!(f, " {}", self.qclass)?;
        write!(f, " {}", self.qtype)?;
        Ok(())
    }
}

// impl fmt::Debug for Question {
//     fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
//         f.debug_struct("Question")
//         .field("qname", &self.qname)
//         .field("qtype", &self.qtype)
//          .field("qclass", &self.qclass)
//          .finish()
//     }
// }

impl From<&Question> for Vec<u8> {
    fn from(v: &Question) -> Self {
        let mut bytes: Vec<u8> = Vec::new();

        bytes.write(Vec::<u8>::from(&v.qname).as_slice());
        bytes.write_u16(u16::from(v.qtype));
        bytes.write_u16(u16::from(v.qclass));

        bytes
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_display() {
        let question = &Question {
            qname: Name::new("example.com."),
            qtype: Type::A,
            qclass: Class::IN,
        };

        assert_eq!(question.to_string(), "example.com. IN A".to_string());
    }

    #[test]
    fn test_into_vec_u8() {
        let question = &Question {
            qname: Name::new("example.com."),
            qtype: Type::A,
            qclass: Class::IN,
        };

        assert_eq!(
            Vec::<u8>::from(question),
            b"\x07example\x03com\x00\x00\x01\x00\x01".as_slice(),
        );
    }
}
