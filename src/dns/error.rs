#[derive(Debug, Eq, PartialEq)]
pub enum Error {
    InvalidClass,
    InvalidOpcode,
    InvalidRCode,
    InvalidRData,
    InvalidType,

    Read,
    Write,

    Deserialization,
}

impl std::error::Error for Error {}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Error::InvalidClass => write!(f, "Invalid Class"),
            Error::InvalidOpcode => write!(f, "Invalid Opcode"),
            Error::InvalidRCode => write!(f, "Invalid RCode"),
            Error::InvalidRData => write!(f, "Invalid RData"),
            Error::InvalidType => write!(f, "Invalid Type"),

            Error::Read => write!(f, "Read"),
            Error::Write => write!(f, "Write"),

            Error::Deserialization => write!(f, "Deserialization Error"),
        }
    }
}
