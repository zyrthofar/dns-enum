use super::io::ByteWrite;

#[derive(Clone, Eq, PartialEq, Hash, Debug)]
pub struct Name(String);

impl Name {
    #[must_use]
    pub fn new(v: &str) -> Self {
        Name::from(v)
    }

    #[must_use]
    pub fn build(labels: &[&str]) -> Self {
        Name::from(labels.join("."))
    }

    #[must_use]
    pub fn parent(&self) -> Option<Name> {
        if self.0 == "." || self.0.is_empty() {
            return None;
        }

        self.0.split_once('.').map(|(_, n)| Self::from(n))
    }

    #[must_use]
    pub fn labels(&self) -> Vec<String> {
        self.0.split('.').map(ToString::to_string).collect()
    }
}

impl From<&str> for Name {
    fn from(v: &str) -> Self {
        Name::from(v.to_string())
    }
}

impl From<String> for Name {
    fn from(v: String) -> Self {
        let mut v = v;
        if !v.ends_with('.') {
            v.push('.');
        }
        Name(v)
    }
}

impl std::fmt::Display for Name {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)?;
        Ok(())
    }
}

impl From<Name> for String {
    fn from(v: Name) -> Self {
        v.0
    }
}

impl From<&Name> for Vec<u8> {
    #![allow(clippy::cast_possible_truncation)]
    fn from(v: &Name) -> Self {
        let mut bytes: Vec<u8> = Vec::new();

        for label in v.0.split('.') {
            if label.is_empty() {
                continue;
            }
            bytes.write_u8(label.len() as u8);
            bytes.write(label.as_bytes());
        }

        // Writes the root zone's empty label length.
        bytes.write_u8(0x00);

        bytes
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_new() {
        assert_eq!(Name::new("example.com."), Name("example.com.".to_string()));
        assert_eq!(Name::new("example.com"), Name("example.com.".to_string()));
        assert_eq!(Name::new("com."), Name("com.".to_string()));
        assert_eq!(Name::new("."), Name(".".to_string()));
    }

    #[test]
    fn test_parent() {
        assert_eq!(
            Name("example.com.".to_string()).parent(),
            Some(Name::new("com."))
        );
        assert_eq!(Name("com.".to_string()).parent(), Some(Name::new(".")));
        assert_eq!(Name(".".to_string()).parent(), None);
        assert_eq!(Name("".to_string()).parent(), None);
    }

    #[test]
    fn test_from_str() {
        assert_eq!(Name::from("example.com."), Name::new("example.com."));
        assert_eq!(Name::from("example.com"), Name::new("example.com."));
        assert_eq!(Name::from("com."), Name::new("com."));
        assert_eq!(Name::from("."), Name::new("."));
    }

    #[test]
    fn test_from_string() {
        assert_eq!(
            Name::from("example.com.".to_string()),
            Name::new("example.com.")
        );
        assert_eq!(
            Name::from("example.com".to_string()),
            Name::new("example.com.")
        );
        assert_eq!(Name::from("com.".to_string()), Name::new("com."));
        assert_eq!(Name::from(".".to_string()), Name::new("."));
    }

    #[test]
    fn test_display() {
        assert_eq!(
            Name::new("example.com.").to_string(),
            "example.com.".to_string()
        );
        assert_eq!(
            Name::new("example.com").to_string(),
            "example.com.".to_string()
        );
        assert_eq!(Name::new("com.").to_string(), "com.".to_string());
        assert_eq!(Name::new(".").to_string(), ".".to_string());
    }

    #[test]
    fn test_into_string() {
        assert_eq!(
            String::from(Name::new("example.com.")),
            "example.com.".to_string()
        );
        assert_eq!(String::from(Name::new("com.")), "com.".to_string());
        assert_eq!(String::from(Name::new(".")), ".".to_string());
    }

    #[test]
    fn test_into_vec_u8() {
        assert_eq!(
            Vec::<u8>::from(&Name::new("example.com.")),
            vec![0x07, 0x65, 0x78, 0x61, 0x6d, 0x70, 0x6c, 0x65, 0x03, 0x63, 0x6f, 0x6d, 0x00]
        );
        assert_eq!(
            Vec::<u8>::from(&Name::new("com.")),
            vec![0x03, 0x63, 0x6f, 0x6d, 0x00]
        );
        assert_eq!(Vec::<u8>::from(&Name::new(".")), vec![0x00]);
    }
}
