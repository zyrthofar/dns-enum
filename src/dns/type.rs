#[derive(Copy, Clone, Eq, PartialEq, Hash, Debug)]
// #[cfg_attr(test, derive())]
#[allow(clippy::upper_case_acronyms)]
pub enum Type {
    A,
    NS,
    MD,
    MF,
    CNAME,
    SOA,
    MB,
    MG,
    MR,
    NULL,
    WKS,
    PTR,
    HINFO,
    MINFO,
    MX,
    TXT,

    // RFC 6891
    OPT,

    // RFC 4034
    RRSIG,
    NSEC,
    DNSKEY,

    // RFC 5155
    NSEC3,
    NSEC3PARAM,

    // QTYPE
    AXFR,
    MAILB,
    MAILA,
    ALL,
}

impl TryFrom<u16> for Type {
    type Error = String;

    fn try_from(v: u16) -> Result<Self, String> {
        match v {
            1 => Ok(Self::A),
            2 => Ok(Self::NS),
            3 => Ok(Self::MD),
            4 => Ok(Self::MF),
            5 => Ok(Self::CNAME),
            6 => Ok(Self::SOA),
            7 => Ok(Self::MB),
            8 => Ok(Self::MG),
            9 => Ok(Self::MR),
            10 => Ok(Self::NULL),
            11 => Ok(Self::WKS),
            12 => Ok(Self::PTR),
            13 => Ok(Self::HINFO),
            14 => Ok(Self::MINFO),
            15 => Ok(Self::MX),
            16 => Ok(Self::TXT),
            41 => Ok(Self::OPT),
            46 => Ok(Self::RRSIG),
            47 => Ok(Self::NSEC),
            48 => Ok(Self::DNSKEY),
            50 => Ok(Self::NSEC3),
            51 => Ok(Self::NSEC3PARAM),
            252 => Ok(Self::AXFR),
            253 => Ok(Self::MAILB),
            254 => Ok(Self::MAILA),
            255 => Ok(Self::ALL),
            _ => Err("error with type: invalid value".to_string()),
        }
    }
}

impl From<Type> for u16 {
    fn from(v: Type) -> Self {
        match v {
            Type::A => 1,
            Type::NS => 2,
            Type::MD => 3,
            Type::MF => 4,
            Type::CNAME => 5,
            Type::SOA => 6,
            Type::MB => 7,
            Type::MG => 8,
            Type::MR => 9,
            Type::NULL => 10,
            Type::WKS => 11,
            Type::PTR => 12,
            Type::HINFO => 13,
            Type::MINFO => 14,
            Type::MX => 15,
            Type::TXT => 16,
            Type::OPT => 41,
            Type::RRSIG => 46,
            Type::NSEC => 47,
            Type::DNSKEY => 48,
            Type::NSEC3 => 50,
            Type::NSEC3PARAM => 51,
            Type::AXFR => 252,
            Type::MAILB => 253,
            Type::MAILA => 254,
            Type::ALL => 255,
        }
    }
}

impl std::fmt::Display for Type {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        let s = match *self {
            Self::A => "A".to_string(),
            Self::NS => "NS".to_string(),
            Self::MD => "MD".to_string(),
            Self::MF => "MF".to_string(),
            Self::CNAME => "CNAME".to_string(),
            Self::SOA => "SOA".to_string(),
            Self::MB => "MB".to_string(),
            Self::MG => "MG".to_string(),
            Self::MR => "MR".to_string(),
            Self::NULL => "NULL".to_string(),
            Self::WKS => "WKS".to_string(),
            Self::PTR => "PTR".to_string(),
            Self::HINFO => "HINFO".to_string(),
            Self::MINFO => "MINFO".to_string(),
            Self::MX => "MX".to_string(),
            Self::TXT => "TXT".to_string(),
            Self::OPT => "OPT".to_string(),
            Self::RRSIG => "RRSIG".to_string(),
            Self::NSEC => "NSEC".to_string(),
            Self::DNSKEY => "DNSKEY".to_string(),
            Self::NSEC3 => "NSEC3".to_string(),
            Self::NSEC3PARAM => "NSEC3PARAM".to_string(),
            Self::AXFR => "AXFR".to_string(),
            Self::MAILB => "MAILB".to_string(),
            Self::MAILA => "MAILA".to_string(),
            Self::ALL => "ALL".to_string(),
        };
        write!(f, "{s}")?;
        Ok(())
    }
}
