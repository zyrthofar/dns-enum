pub mod class;
pub mod error;
pub mod io;
pub mod message;
pub mod name;
pub mod nsec3;
pub mod opcode;
pub mod question;
pub mod rcode;
pub mod rdata;
pub mod resource_record;
pub mod r#type;

use std::net::UdpSocket;

use bytes::Bytes;

pub use self::class::Class;
pub use self::error::Error;
pub use self::message::Message;
pub use self::name::Name;
pub use self::nsec3::*;
pub use self::opcode::Opcode;
pub use self::question::Question;
pub use self::r#type::Type;
pub use self::rcode::RCode;
pub use self::rdata::RData;
pub use self::resource_record::ResourceRecord;

/// # Errors
///
/// Will return `Err` if anything goes wrong during the DNS query.
pub fn send(msg: Message, nameserver: &str) -> Result<Message, String> {
    let req_body = Vec::<u8>::from(msg);
    send_bytes(req_body.as_slice(), nameserver)
}

/// # Errors
///
/// Will return `Err` if anything goes wrong during the DNS query.
pub fn send_bytes(req_body: &[u8], nameserver: &str) -> Result<Message, String> {
    let socket = UdpSocket::bind("0.0.0.0:0").map_err(|e| format!("error binding: {e}"))?;
    let _ = socket
        .send_to(req_body.as_ref(), nameserver)
        .map_err(|e| format!("error sending bytes: {e}"))?;

    let mut buf = [0; 4096];
    let c = socket
        .recv(&mut buf)
        .map_err(|e| format!("error receiving bytes: {e}"))?;

    let resp_body = Bytes::copy_from_slice(buf[..c].as_ref()).to_vec();
    let msg = Message::try_from(resp_body)
        .map_err(|e| format!("error converting bytes to message: {e}"))?;

    Ok(msg)
}

#[cfg(test)]
mod tests {
    // use super::*;

    #[test]
    fn test_send() {}
}
