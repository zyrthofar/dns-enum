use std::fs::File;
use std::io::{self, BufRead, Cursor, Write};
use std::iter::once;
use std::thread;
use std::time::{Duration, Instant};

use data_encoding as enc;
use uuid::Uuid;

use dns::io::ByteRead;

mod database;
use database::{Database, Domain, Nsec3Record, Zone};

const BASE_NAME: &str = "example.com.";
// const BASE_NAME: &str = "protonmail.com.";
// const BASE_NAME: &str = "verisign.com.";
// const BASE_NAME: &str = "verisign.net.";

// const NAMESERVER: &str = "1.1.1.1:53";
const NAMESERVER: &str = "8.8.8.8:53";
// const NAMESERVER: &str = "192.168.1.1:53";

// Available files using n0kovo's dictionaries:
// * n0kovo_subdomains_tiny.txt,      50,000 lines
// * n0kovo_subdomains_small.txt,    200,000 lines
// * n0kovo_subdomains_medium.txt,   500,000 lines
// * n0kovo_subdomains_large.txt,  1,000,000 lines
// * n0kovo_subdomains_huge.txt,   3,000,000 lines
const DICTIONARY_FILE: &str = "n0kovo_subdomains_tiny.txt";

fn main() -> Result<(), String> {
    let mut app = App::new(BASE_NAME).map_err(|e| format!("error configuring application: {e}"))?;
    let mut chain = dns::nsec3::Chain::new();

    println!("Loading existing NSEC3 hashes from database...");
    app.load_nsec3_hashes(&mut chain)
        .map_err(|e| format!("error retrieving NSEC3 hashes from database: {e}"))?;
    let hash_count_from_database = chain.len();
    println!("Loaded {hash_count_from_database} NSEC3 hashes.");

    println!();
    if chain.is_complete() {
        println!("All NSEC3 hashes found.");
    } else {
        println!("Enumerating NSEC3 hashes by trying random names...");
        app.enumerate_nsec3_hashes(&mut chain)
            .map_err(|e| format!("error performing hash enumeration: {e}"))?;
        let hash_count_from_enumeration = chain.len();
        println!(
            "Enumerated {} NSEC3 hashes.",
            hash_count_from_enumeration - hash_count_from_database
        );
    }

    let records = chain.records();
    let mut data = records
        .iter()
        .map(|r| (r.hash.clone(), r, None::<Domain>))
        .collect::<Vec<(Vec<u8>, &dns::nsec3::Record, Option<Domain>)>>();

    println!();
    println!("Loading existing domains from database...");
    app.load_domains(&mut data)
        .map_err(|e| format!("error retrieving domains from database: {e}"))?;
    let domain_count_from_database = data.iter().filter(|d| d.2.is_some()).count();
    println!(
        "Loaded {domain_count_from_database} domains, {} missing.",
        chain.len() - domain_count_from_database
    );

    println!();
    println!("Matching NSEC3 hashes to domain names by performing a dictionary attack...");
    app.do_dictionary_attack(DICTIONARY_FILE, &mut data)
        .map_err(|e| format!("error performing dictionary attack: {e}"))?;
    let domain_count_from_dictionary_attack =
        data.iter().filter(|d| d.2.is_some()).count() - domain_count_from_database;
    println!("Found {domain_count_from_dictionary_attack} domains.");

    println!();
    println!("Matching NSEC3 hashes to domain names by performing a brute-force attack...");
    app.do_brute_force_attack(&mut data)
        .map_err(|e| format!("error performing brute-force attack: {e}"))?;
    let domain_count_from_brute_force_attack =
        data.iter().filter(|d| d.2.is_some()).count() - domain_count_from_dictionary_attack;
    println!("Found {domain_count_from_brute_force_attack} domains.");

    println!();
    println!(
        "Found {}/{} domains:",
        data.iter().filter(|d| d.2.is_some()).count(),
        data.len()
    );
    for d in &data {
        println!(
            "{} -> {}",
            enc::BASE32HEX.encode(&d.0.clone()),
            d.2.as_ref()
                .map_or_else(String::new, |v| v.name.to_string()),
        );
    }

    Ok(())
}

struct App {
    db: Database,
    zone: Zone,
    hasher: dns::nsec3::Hasher,
}

impl App {
    fn new(base_name: &str) -> Result<Self, String> {
        let mut db = Database::connect()?;
        let name = dns::Name::new(base_name);

        let zone = if let Some(z) = db
            .get_zone_by_name(&name)
            .map_err(|e| format!("error retrieving zone from database: {e}"))?
        {
            z
        } else {
            // Retrieves the NSEC3PARAM records of the domain name, as a starting point.
            let msg = dns::Message::new_query(
                dns::Name::new(base_name),
                dns::Type::NSEC3PARAM,
                dns::Class::IN,
            );
            let resp = dns::send(msg, NAMESERVER)
                .map_err(|e| format!("error querying for NSEC3PARAM: {e}"))?;
            let record = resp
                .answers
                .iter()
                .find(|rr| rr.r#type == dns::Type::NSEC3PARAM)
                .ok_or("DNS query did not receive any NSEC3PARAM records")?;

            let mut cursor = Cursor::new(record.rdata.clone());
            let nsec3_alg = cursor
                .read_u8()
                .map_err(|e| format!("error reading algorithm: {e}"))?;
            _ = cursor.read_u8();
            let nsec3_its = cursor
                .read_u16()
                .map_err(|e| format!("error reading iterations: {e}"))?;
            let nsec3_salt_length = cursor
                .read_u8()
                .map_err(|e| format!("error reading salt length: {e}"))?
                as usize;
            let mut nsec3_salt = vec![0; nsec3_salt_length];
            cursor
                .read(&mut nsec3_salt)
                .map_err(|_| "error reading salt".to_string())?;

            let mut z = Zone {
                id: Uuid::nil(),
                name,
                nsec3_alg,
                nsec3_its,
                nsec3_salt,
            };

            let id = db
                .create_zone(&z)
                .map_err(|e| format!("error creating zone: {e}"))?;
            z.id = id;

            z
        };

        let hasher =
            dns::nsec3::Hasher::new(zone.nsec3_alg, zone.nsec3_its, zone.nsec3_salt.clone())
                .map_err(|e| format!("error creating hasher: {e}"))?;

        Ok(App { db, zone, hasher })
    }

    fn load_nsec3_hashes(&mut self, chain: &mut dns::nsec3::Chain) -> Result<(), String> {
        for r in self
            .db
            .get_nsec3_records(&self.zone.id)
            .map_err(|e| format!("error retrieving NSEC3 records from database: {e}"))?
        {
            let record = dns::ResourceRecord {
                name: dns::Name::build(&[
                    enc::BASE32HEX.encode(r.hash.as_slice()).as_str(),
                    self.zone.name.to_string().as_str(),
                ]),
                r#type: dns::Type::NSEC3,
                class: dns::Class::IN,
                ttl: 0,
                rdata: r.rdata,
            };

            let nsec3_record = dns::nsec3::Record::try_from(&record)
                .map_err(|e| format!("error conveting record NSEC3 record: {e}"))?;

            chain
                .add_record(nsec3_record)
                .map_err(|e| format!("error adding NSEC3 records to chain: {e}"))?;
        }

        Ok(())
    }

    fn enumerate_nsec3_hashes(&mut self, chain: &mut dns::nsec3::Chain) -> Result<(), String> {
        if chain.is_complete() {
            return Ok(());
        }

        let mut i = 0usize;
        let mut start = Instant::now().checked_sub(Duration::new(1, 0)).unwrap();
        loop {
            let name = &dns::Name::build(&[
                Uuid::new_v4().to_string().as_str(),
                &self.zone.name.to_string().as_str(),
            ]);
            let hash = self.hasher.hash(name);

            // Spins a bar every 4,096 hashes.
            if i.trailing_zeros() >= 12 {
                let s = ["|", "/", "-", "\\"][(i >> 12) & 0x03];
                print!(
                    "\r{} (try {}, {:.0} hashes/s)",
                    s,
                    i + 1,
                    4096f64 / start.elapsed().as_secs_f64()
                );
                std::io::stdout().flush().unwrap();
                start = Instant::now();
            }

            i += 1;

            if chain.is_accounted_for(&hash) {
                continue;
            }

            let resp = query_dns(name);
            for rr in resp
                .authorities
                .iter()
                .filter(|rr| rr.r#type == dns::Type::NSEC3)
            {
                let nsec3_record = dns::nsec3::Record::try_from(rr)
                    .map_err(|e| format!("error converting NSEC3 record: {e}"))?;

                _ = self.db.create_nsec3_record(&mut Nsec3Record {
                    id: Uuid::nil(),
                    zone_id: self.zone.id,
                    hash: nsec3_record.hash.clone(),
                    next_hash: nsec3_record.next_hash.clone(),
                    rdata: rr.rdata.clone(),
                });

                if !chain.has_record_with_hash(&nsec3_record.hash) {
                    println!(
                        "\r\x1b[92m✓\x1b[0m {}",
                        enc::BASE32HEX.encode(nsec3_record.hash.clone().as_slice())
                    );
                }

                chain.add_record(nsec3_record)?;
            }

            if chain.is_complete() {
                break;
            }

            thread::sleep(Duration::from_millis(1000));
        }

        print!("\r");
        std::io::stdout().flush().unwrap();

        Ok(())
    }

    fn load_domains(
        &mut self,
        data: &mut [(Vec<u8>, &dns::nsec3::Record, Option<Domain>)],
    ) -> Result<(), String> {
        let domains = self
            .db
            .get_domains(&self.zone.id)
            .map_err(|e| format!("error retrieving domains: {e}"))?;
        for d in domains {
            let hash = self.hasher.hash(&d.name);

            if let Some(item) = data.iter_mut().find(|d| d.0 == hash) {
                item.2 = Some(d);
            }
        }

        Ok(())
    }

    fn do_dictionary_attack(
        &mut self,
        file_path: &str,
        data: &mut [(Vec<u8>, &dns::nsec3::Record, Option<Domain>)],
    ) -> Result<(), String> {
        if data.iter().all(|d| d.2.is_some()) {
            return Ok(());
        }

        let dict = File::open(file_path).expect("to open file");
        let names = io::BufReader::new(dict).lines();

        // Loops through the base name, followed by all sub-domains in the dictionary.
        let mut start = Instant::now().checked_sub(Duration::new(1, 0)).unwrap();
        let zone_name = self.zone.name.to_string();
        for (i, name) in once(dns::Name::from(zone_name.as_str()))
            .chain(once(dns::Name::build(&["*", zone_name.as_str()])))
            .chain(
                names
                    .map_while(Result::ok)
                    .map(|n| dns::Name::build(&[n.as_str(), zone_name.as_str()])),
            )
            .enumerate()
        {
            // Spins a bar every 4,096 lines.
            if i.trailing_zeros() >= 12 {
                let s = ["|", "/", "-", "\\"][(i >> 12) & 0x03];
                print!(
                    "\r{} (line {}, {:.0} hashes/s)",
                    s,
                    i + 1,
                    4096f64 / start.elapsed().as_secs_f64()
                );
                std::io::stdout().flush().unwrap();
                start = Instant::now();
            }

            if self
                .add_domain(&name, data)
                .map_err(|e| format!("error adding domain name: {e}"))?
            {
                break;
            }
        }

        print!("\r");
        std::io::stdout().flush().unwrap();

        Ok(())
    }

    fn do_brute_force_attack(
        &mut self,
        data: &mut [(Vec<u8>, &dns::nsec3::Record, Option<Domain>)],
    ) -> Result<(), String> {
        if data.iter().all(|d| d.2.is_some()) {
            return Ok(());
        }

        let alphabet = "abcdefghijklmnopqrstuvwxyz0123456789-".as_bytes();

        let mut i = 0;
        let mut start = Instant::now().checked_sub(Duration::new(1, 0)).unwrap();
        let zone_name = self.zone.name.to_string();
        loop {
            // TODO: Make library for brute-force iterator with alphabet.
            let mut j = i;
            let mut label = vec![];
            loop {
                let letter = alphabet[j % alphabet.len()];
                label.push(letter);

                j /= alphabet.len();
                if j == 0 {
                    break;
                }
                j -= 1;
            }

            label.reverse();
            let label: String = label.iter().map(|x| char::from(*x)).collect();

            // Spins a bar every 4,096 lines.
            if i.trailing_zeros() >= 12 {
                let s = ["|", "/", "-", "\\"][(i >> 12) & 0x03];
                print!(
                    "\r{} (current {}, {:.0} hashes/s)",
                    s,
                    label,
                    4096f64 / start.elapsed().as_secs_f64()
                );
                std::io::stdout().flush().unwrap();
                start = Instant::now();
            }

            let name = dns::Name::build(&[label.as_str(), zone_name.as_str()]);
            if self
                .add_domain(&name, data)
                .map_err(|e| format!("error adding domain name: {e}"))?
            {
                break;
            }

            i += 1;
        }

        print!("\r");
        std::io::stdout().flush().unwrap();

        Ok(())
    }

    fn add_domain(
        &mut self,
        name: &dns::Name,
        data: &mut [(Vec<u8>, &dns::nsec3::Record, Option<Domain>)],
    ) -> Result<bool, String> {
        let hash = self.hasher.hash(name);

        if let Some(d) = data.iter_mut().find(|v| v.0 == hash) {
            if d.2.is_some() {
                return Ok(false);
            }

            print!(
                "\r\x1b[92m✓\x1b[0m {} -> {}",
                enc::BASE32HEX.encode(hash.as_slice()),
                name
            );
            std::io::stdout().flush().unwrap();

            let nsec3_record = self
                .db
                .get_nsec3_record_by_hash(&self.zone.id, &hash)
                .map_err(|e| format!("error retrieving nsec3 record: {e}"))?;

            let mut domain = Domain {
                id: Uuid::nil(),
                zone_id: self.zone.id,
                nsec3_record_id: nsec3_record.map(|r| r.id),
                name: name.clone(),
            };

            self.db
                .create_domain(&mut domain)
                .map_err(|e| format!("error creating domain: {e}"))?;
            d.2 = Some(domain);

            for t in &d.1.types {
                print!(" {t}");
                std::io::stdout().flush().unwrap();
            }

            println!();

            if data.iter().all(|d| d.2.is_some()) {
                return Ok(true);
            }
        }

        Ok(false)
    }
}

fn query_dns(name: &dns::Name) -> dns::Message {
    // TODO: Implement OPT records during query, with DNSSEC flag.
    // let req = dns::Message::new_query(name.clone(), dns::Type::NSEC3, dns::Class::IN);

    // Creates request body headers, with 1 question and 1 additional records.
    let mut req_body = vec![
        0x01u8, 0x23, 0x01, 0x20, 0x00, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x01,
    ];

    // Adds the question record, querying for NSEC3 records.
    req_body.append(&mut Vec::<u8>::from(name));
    req_body.append(&mut vec![0x00, 0x32, 0x00, 0x01]);

    // Adds OPT record indicating dnssec.
    req_body.append(&mut vec![
        0x00, 0x00, 0x29, 0x10, 0x00, 0x00, 0x00, 0x80, 0x00, 0x00, 0x00,
    ]);

    dns::send_bytes(&req_body, NAMESERVER).expect("dns::query")
}
