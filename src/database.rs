use postgres::{Client, NoTls};
use uuid::Uuid;

#[derive(Debug)]
pub struct Zone {
    pub id: Uuid,
    pub name: dns::Name,
    pub nsec3_alg: u8,
    pub nsec3_its: u16,
    pub nsec3_salt: Vec<u8>,
}

#[derive(Debug)]
pub struct Domain {
    pub id: Uuid,
    pub zone_id: Uuid,
    pub nsec3_record_id: Option<Uuid>,
    pub name: dns::Name,
}

#[derive(Debug)]
pub struct Record {
    pub id: Uuid,
    pub domain_id: Uuid,
    pub name: dns::Name,
    pub r#type: dns::Type,
    pub class: dns::Class,
    pub ttl: u32,
    pub rdata: Vec<u8>,
}

#[derive(Debug)]
pub struct Nsec3Record {
    pub id: Uuid,
    pub zone_id: Uuid,
    pub hash: Vec<u8>,
    pub next_hash: Vec<u8>,
    pub rdata: Vec<u8>,
}

pub struct Database {
    client: Client,
}

impl Database {
    pub fn connect() -> Result<Self, String> {
        let client = Client::connect("host=localhost user=dns password=dns", NoTls)
            .map_err(|e| format!("error creating client: {e}"))?;

        Ok(Database { client })
    }

    #[allow(clippy::cast_possible_truncation)]
    #[allow(clippy::cast_sign_loss)]
    pub fn get_zone_by_name(&mut self, name: &dns::Name) -> Result<Option<Zone>, String> {
        Ok(self.client
            .query(
                "SELECT id, name, nsec3_alg, nsec3_its, nsec3_salt FROM zones WHERE name = $1 LIMIT 1;",
                &[
                    &name.to_string(),
                ],
            ).map_err(|e| format!("database error: {e}"))?
            .first()
            .map(|r| {
                Zone {
                    id: r.get("id"),
                    name: dns::Name::new(r.get("name")),
                    nsec3_alg: r.get::<&str, i32>("nsec3_alg") as u8,
                    nsec3_its: r.get::<&str, i32>("nsec3_its") as u16,
                    nsec3_salt: r.get("nsec3_salt"),
                }
            }))
    }

    pub fn create_zone(&mut self, zone: &Zone) -> Result<Uuid, String> {
        Ok(self.client
            .query(
                "INSERT INTO zones (name, nsec3_alg, nsec3_its, nsec3_salt) VALUES ($1, $2, $3, $4) RETURNING id;",
                &[
                    &zone.name.to_string(),
                    &i32::from(zone.nsec3_alg),
                    &i32::from(zone.nsec3_its),
                    &zone.nsec3_salt,
                ],
            ).map_err(|e| format!("database error: {e}"))?
            .first()
            .ok_or("already exists".to_string())?
            .get("id"))
    }

    pub fn get_nsec3_records(&mut self, zone_id: &Uuid) -> Result<Vec<Nsec3Record>, String> {
        Ok(self
            .client
            .query(
                "SELECT id, zone_id, hash, next_hash, rdata FROM nsec3_records WHERE zone_id = $1;",
                &[&zone_id],
            )
            .map_err(|e| format!("database error: {e}"))?
            .iter()
            .map(|r| Nsec3Record {
                id: r.get("id"),
                zone_id: r.get("zone_id"),
                hash: r.get("hash"),
                next_hash: r.get("next_hash"),
                rdata: r.get("rdata"),
            })
            .collect())
    }

    pub fn get_nsec3_record_by_hash(
        &mut self,
        zone_id: &Uuid,
        hash: &Vec<u8>,
    ) -> Result<Option<Nsec3Record>, String> {
        Ok(self.client
            .query(
                "SELECT id, zone_id, hash, next_hash, rdata FROM nsec3_records WHERE zone_id = $1 AND hash = $2;",
                &[
                    &zone_id,
                    &hash,
                ],
            ).map_err(|e| format!("database error: {e}"))?
            .first()
            .map(|r| {
                Nsec3Record {
                    id: r.get("id"),
                    zone_id: r.get("zone_id"),
                    hash: r.get("hash"),
                    next_hash: r.get("next_hash"),
                    rdata: r.get("rdata"),
                }
            }))
    }

    pub fn create_nsec3_record(&mut self, nsec3_record: &mut Nsec3Record) -> Result<(), String> {
        let id = self.client
            .query(
                "INSERT INTO nsec3_records (zone_id, hash, next_hash, rdata) VALUES ($1, $2, $3, $4) ON CONFLICT DO NOTHING RETURNING id;",
                &[
                    &nsec3_record.zone_id,
                    &nsec3_record.hash,
                    &nsec3_record.next_hash,
                    &nsec3_record.rdata,
                ],
            ).map_err(|e| format!("database error: {e}"))?
            .first()
            .ok_or("already exists".to_string())?
            .get("id");

        nsec3_record.id = id;

        Ok(())
    }

    pub fn get_domains(&mut self, zone_id: &Uuid) -> Result<Vec<Domain>, String> {
        Ok(self
            .client
            .query(
                "SELECT id, zone_id, nsec3_record_id, name FROM domains WHERE zone_id = $1;",
                &[&zone_id],
            )
            .map_err(|e| format!("database error: {e}"))?
            .iter()
            .map(|r| Domain {
                id: r.get("id"),
                zone_id: r.get("zone_id"),
                nsec3_record_id: r.get("nsec3_record_id"),
                name: dns::Name::new(r.get("name")),
            })
            .collect())
    }

    pub fn create_domain(&mut self, domain: &mut Domain) -> Result<(), String> {
        let id = self.client
            .query(
                "INSERT INTO domains (zone_id, nsec3_record_id, name) VALUES ($1, $2, $3) ON CONFLICT DO NOTHING RETURNING id;",
                &[
                    &domain.zone_id,
                    &domain.nsec3_record_id,
                    &domain.name.to_string(),
                ],
            ).map_err(|e| format!("database error: {e}"))?
            .first()
            .ok_or("already exists".to_string())?
            .get("id");

        domain.id = id;

        Ok(())
    }

    // pub fn create_record(&mut self, record: &mut Record) -> Result<(), String> {
    //     let id = self.client
    //         .query(
    //             "INSERT INTO records (domain_id, name, type, class, ttl, rdata) VALUES ($1, $2, $3, $4, $5, $6) RETURNING id;",
    //             &[
    //                 &record.domain_id,
    //                 &record.name.to_string(),
    //                 &record.r#type.to_string(),
    //                 &record.class.to_string(),
    //                 &(record.ttl as i32),
    //                 &record.rdata,
    //             ],
    //         ).map_err(|e| format!("database error: {e}"))?
    //         .first()
    //         .ok_or("already exists".to_string())?
    //         .get("id");
    //     record.id = id;

    //     Ok(())
    // }
}
